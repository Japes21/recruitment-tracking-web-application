const Config = {
  // serverURL: "http://172.25.60.15:4000", //IOT Shubham
  // serverURL: "http://172.25.60.39:4000", //MEG-Nxt_Dev Shubham
  serverURL: "http://localhost:4000",
  // serverURL: "http://172.25.60.36:4000", //MEG-Nxt_Dev
  // serverURL: "http://172.25.20.60:4000", //MEG-Nxt_Dev Linux-Ubuntu Server
};

export default Config;
