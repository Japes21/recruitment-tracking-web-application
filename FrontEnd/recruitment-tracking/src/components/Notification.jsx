import React from "react";
import { MdOutlineCancel } from "react-icons/md";
const chatData = [
  {
    message: "Roman Joined the Team!",

    desc: "Congratulate him",

    time: "9:08 AM",
  },

  {
    message: "New message received",

    desc: "Salma sent you new message",

    time: "11:56 AM",
  },

  {
    message: "New Payment received",

    desc: "Check your earnings",

    time: "4:39 AM",
  },

  {
    message: "Jolly completed tasks",

    desc: "Assign her new tasks",

    time: "1:12 AM",
  },
];
const Notification = () => {
  return (
    <div className="z-50 nav-item absolute right-5 md:right-40 top-16 bg-white dark:bg-[#42464D] p-8 rounded-lg w-96">
      <div className="flex justify-between items-center">
        <div className="flex gap-3">
          <p className="font-semibold text-lg dark:text-gray-200">
            Notifications
          </p>
          <button
            type="button"
            className="text-white text-xs rounded p-1 px-2 bg-orange-theme "
          >
            {" "}
            5 New
          </button>
        </div>
        <button className="text-2xl rounded-full hover:bg-slate-300 text-slate-500">
          <MdOutlineCancel />
        </button>
      </div>
      <div className="mt-5 ">
        {chatData?.map((item, index) => (
          <div
            key={index}
            className="flex items-center leading-8 gap-5 border-b-1 border-color p-3"
          >
            <div>
              <p className="font-semibold dark:text-gray-200">{item.message}</p>
              <p className="text-gray-500 text-sm dark:text-gray-400">
                {" "}
                {item.desc}{" "}
              </p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Notification;
