import { NavLink, useNavigate } from "react-router-dom";
import logo1 from "../images/logo1.png";
import { AiFillHome } from "react-icons/ai";
import { AiOutlineHome } from "react-icons/ai";
import { FaUsers } from "react-icons/fa";
import { FaLocationDot } from "react-icons/fa6";
import { TbManualGearbox } from "react-icons/tb";
import { ImProfile } from "react-icons/im";
import { BsFillExclamationSquareFill } from "react-icons/bs";
import { AiTwotoneSnippets } from "react-icons/ai";
import { BsPersonVcardFill } from "react-icons/bs";
import { IoIosCreate } from "react-icons/io";
import { ImAttachment } from "react-icons/im";

const Sidebar = ({ role }) => {
  const getMenuItems = () => {
    if (role === "admin") {
      return [
        {
          title: "Admin Home",
          icon: <AiFillHome />,
          path: "/homePage",
        },
        {
          title: "User Management",
          icon: <FaUsers />,
          path: "/userManagement",
        },
        {
          title: "Location Management",
          icon: <FaLocationDot />,
          path: "/locationManagement",
        },
        {
          title: "Domain Management",
          icon: <TbManualGearbox />,
          path: "/domainManagement",
        },
        {
          title: "Position Management",
          icon: <ImProfile />,
          path: "/designationManagement",
        },
        {
          title: "Request Status Management",
          icon: <BsFillExclamationSquareFill />,
          path: "/requestStatusManagement",
        },
        {
          title: "Candidate Status Management",
          icon: <BsFillExclamationSquareFill />,
          path: "/candidateStatusManagement",
        },
        {
          title: "Request Management",
          icon: <AiTwotoneSnippets />,
          path: "/requestManagement",
        },
        {
          title: "Candidate Management",
          icon: <BsPersonVcardFill />,
          path: "/candidateManagement",
        },
      ];
    } else if (role === "requester") {
      return [
        {
          title: "Requester Home",
          icon: <AiFillHome />,
          path: "/homePage",
        },
        {
          title: "Create Request",
          icon: <IoIosCreate />,
          path: "/createRequest",
        },
        {
          title: "Request Management",
          icon: <AiTwotoneSnippets />,
          path: "/requestManagement",
        },
        {
          title: "Candidate Management",
          icon: <BsPersonVcardFill />,
          path: "/candidateManagement",
        },
      ];
    } else if (role === "hr") {
      return [
        {
          title: "HR Home",
          icon: <AiFillHome />,
          path: "/homePage",
        },
        {
          title: "Request Management",
          icon: <AiTwotoneSnippets />,
          path: "/requestManagement",
        },
        {
          title: "Candidate Management",
          icon: <BsPersonVcardFill />,
          path: "/candidateManagement",
        },
      ];
    }
    return [];
  };

  const menuItems = getMenuItems();

  const navigate = useNavigate();

  const handleNavigation = (path) => {
    navigate(path);
  };

  const activeLink =
    "bg-customBlue text-white cursor-pointer w-52 flex flex-row items-center gap-2 text-black font-semibold text-left p-1 rounded-md hover:bg-customBlue hover:text-white active:bg-gray-500 focus:outline-none focus:ring focus:ring-white";

  const normalLink =
    "cursor-pointer w-52 flex flex-row items-center gap-2 text-black font-semibold text-left p-1 rounded-md hover:bg-customBlue hover:text-white active:bg-gray-500 focus:outline-none focus:ring focus:ring-white";

  return (
    <div className="h-screen w-56 drop-shadow-2xl bg-white ">
      <div className="bg-white py-1 h-14 flex justify-center border-b-2 border-white">
        <img className="h-12 w-auto" src={logo1} />
      </div>
      <div className="flex p-4">
        <ul className="space-y-4">
          {menuItems.map((item, index) => (
            <NavLink
              to={item.path}
              key={index}
              className={({ isActive }) => (isActive ? activeLink : normalLink)}
            >
              <span>{item.icon}</span>
              <span>{item.title}</span>
            </NavLink>
            // <NavLink
            //   to={item.path}
            //   activeClassName="bg-customBlue" // This class will be applied to the active link
            //   className="cursor-pointer w-52 flex flex-row items-center gap-2 text-black font-semibold text-left p-1 rounded-md hover:bg-customBlue hover:text-white active:bg-gray-500 focus:outline-none focus:ring focus:ring-white" // Ensure the NavLink covers the entire block
            //   key={index}
            // >
            // <span>{item.icon}</span>
            // <span>{item.title}</span>
            // </NavLink>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default Sidebar;
