import React, { useState, useEffect } from "react";
import Modal from "react-modal";
import "./dynamicSearchCard.css";
import { toast } from "react-toastify";
import Config from "../config";
import axios from "axios";

const DynamicSearchCard = ({ search, searchBy, onFind }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [searchValue, setSearchValue] = useState("");

  const [roles, setRoles] = useState([]);
  const [locations, setLocations] = useState([]);
  const [domains, setDomains] = useState([]);
  const [designations, setDesignations] = useState([]);
  const [requestStatuses, setRequestStatuses] = useState([]);
  const [candidateStatuses, setCandidateStatuses] = useState([]);

  useEffect(() => {
    axios
      .get(Config.serverURL + "/admin/getAllRoles")
      .then((response) => {
        setRoles(response.data);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });

    axios
      .get(Config.serverURL + "/admin/getAllLocations")
      .then((response) => {
        setLocations(response.data);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });

    axios
      .get(Config.serverURL + "/admin/getAllDomains")
      .then((response) => {
        setDomains(response.data);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });

    axios
      .get(Config.serverURL + "/admin/getAllDesignations")
      .then((response) => {
        setDesignations(response.data);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });

    axios
      .get(Config.serverURL + "/admin/getAllRequestStatus")
      .then((response) => {
        setRequestStatuses(response.data);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });

    axios
      .get(Config.serverURL + "/admin/getAllCandidateStatus")
      .then((response) => {
        setCandidateStatuses(response.data);
      });
  }, []);

  const handleButtonClick = () => {
    setIsModalOpen(true);
  };

  const handleModalClose = () => {
    setIsModalOpen(false);
    setSearchValue("");
  };

  const handleInputChange = (e) => {
    setSearchValue(e.target.value);
  };

  const handleFind = () => {
    if (searchValue.length === 0) {
      toast.error("please enter the value to search!!!!!");
    } else {
      onFind(searchBy, searchValue);
      handleModalClose();
    }
  };

  function tag() {
    switch (searchBy) {
      case "Role":
        return (
          <select
            value={searchValue}
            onChange={handleInputChange}
            className="w-full px-3 py-2 border rounded focus:outline-none focus:border-black"
          >
            <option value="">Select Role</option>
            {roles.map((role, index) => (
              <option key={role.role_name} value={role.role_name}>
                {role.role_name}
              </option>
            ))}
          </select>
        );

      case "Location":
        return (
          <select
            value={searchValue}
            onChange={handleInputChange}
            className="w-full px-3 py-2 border rounded focus:outline-none focus:border-black"
          >
            <option value="">Select Location</option>
            {locations.map((loc, index) => (
              <option key={loc.location_name} value={loc.location_name}>
                {loc.location_name}
              </option>
            ))}
          </select>
        );

      case "Domain":
        return (
          <select
            value={searchValue}
            onChange={handleInputChange}
            className="w-full px-3 py-2 border rounded focus:outline-none focus:border-black"
          >
            <option value="">Select Domain</option>
            {domains.map((dom, index) => (
              <option key={dom.domain_name} value={dom.domain_name}>
                {dom.domain_name}
              </option>
            ))}
          </select>
        );

      case "Position":
        return (
          <select
            value={searchValue}
            onChange={handleInputChange}
            className="w-full px-3 py-2 border rounded focus:outline-none focus:border-black"
          >
            <option value="">Select Position</option>
            {designations.map((des, index) => (
              <option key={des.designation_name} value={des.designation_name}>
                {des.designation_name}
              </option>
            ))}
          </select>
        );

      case "Request Status":
        return (
          <select
            value={searchValue}
            onChange={handleInputChange}
            className="w-full px-3 py-2 border rounded focus:outline-none focus:border-black"
          >
            <option value="">Select Request Status</option>
            {requestStatuses.map((req, index) => (
              <option key={req.rq_st_name} value={req.rq_st_name}>
                {req.rq_st_name}
              </option>
            ))}
          </select>
        );

      case "Candidate Status":
        return (
          <select
            value={searchValue}
            onChange={handleInputChange}
            className="w-full px-3 py-2 border rounded focus:outline-none focus:border-black"
          >
            <option value="">Select Candidate Status</option>
            {candidateStatuses.map((cnd, index) => (
              <option key={cnd.cnd_st_name} value={cnd.cnd_st_name}>
                {cnd.cnd_st_name}
              </option>
            ))}
          </select>
        );

      default:
        return (
          <input
            type="text"
            value={searchValue}
            onChange={handleInputChange}
            className="w-full px-3 py-2 border rounded focus:outline-none focus:border-black"
          />
        );
    }
  }

  return (
    <div>
      <button
        className="text-white font-semibold text-center p-1 rounded-full bg-gray-800 hover:bg-gray-600 active:bg-gray-500 focus:outline-none focus:ring focus:ring-white"
        onClick={handleButtonClick}
      >
        Find by {searchBy}
      </button>

      <Modal
        isOpen={isModalOpen}
        onRequestClose={handleModalClose}
        className="modal-content"
        overlayClassName="modal-overlay"
      >
        <div className="mt-4 p-4 bg-white rounded shadow-md">
          <label className="block text-gray-700 font-bold mb-2">
            Enter {searchBy}:
          </label>
          {/* <input
            type="text"
            value={searchValue}
            onChange={handleInputChange}
            className="w-full px-3 py-2 border rounded focus:outline-none focus:border-black"
          /> */}
          {tag()}
          <button
            className="mt-4 text-white font-semibold text-center p-1 rounded-full bg-gray-800 hover:bg-gray-600 active:bg-gray-500 focus:outline-none focus:ring focus:ring-white"
            onClick={handleFind}
          >
            Find {search}
          </button>
        </div>
      </Modal>
    </div>
  );
};

export default DynamicSearchCard;
