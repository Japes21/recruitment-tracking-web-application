import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Config from "../config";
import axios from "axios";
import { toast } from "react-toastify";
import { ImAttachment } from "react-icons/im";
import Modal from "react-modal";
import { useNavigate } from "react-router-dom";
import { FaDownload } from "react-icons/fa";
import { GrEdit } from "react-icons/gr";
import { BsDashLg } from "react-icons/bs";
import { MdDeleteSweep } from "react-icons/md";

const DynamicTable = ({ title, columnsTitles, rows, rowLoader }) => {
  const [reqStatuses, setReqStatuses] = useState([]);
  const [cndStatuses, setCndStatuses] = useState([]);
  const [locations, setLocations] = useState([]);
  const [domains, setDomains] = useState([]);
  const [designations, setDesignations] = useState([]);
  const [roles, setRoles] = useState([]);

  useEffect(() => {
    axios
      .get(Config.serverURL + "/admin/getAllLocations")
      .then((response) => {
        setLocations(response.data);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });

    axios
      .get(Config.serverURL + "/admin/getAllDomains")
      .then((response) => {
        setDomains(response.data);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });

    axios
      .get(Config.serverURL + "/admin/getAllDesignations")
      .then((response) => {
        setDesignations(response.data);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });

    axios
      .get(Config.serverURL + "/admin/getAllRequestStatus")
      .then((response) => {
        setReqStatuses(response.data);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });

    axios
      .get(Config.serverURL + "/admin/getAllCandidateStatus")
      .then((response) => {
        setCndStatuses(response.data);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });

    axios
      .get(Config.serverURL + "/admin/getAllRoles")
      .then((response) => {
        setRoles(response.data);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });
  }, []);

  const [candidateName, setCandidateName] = useState("");
  const [userId] = useState(sessionStorage.getItem("userId"));
  const [resume, setResume] = useState();
  const [requestId, setRequestId] = useState(0);
  const [userName] = useState(sessionStorage.getItem("userName"));
  const [requesterId, setRequesterId] = useState(0);
  const [requesterName, setRequesterName] = useState("");

  const [location, setLocation] = useState("");
  const [domain, setDomain] = useState("");
  const [designation, setDesignation] = useState("");
  const [headCount, setHeadCount] = useState("");
  const [deadline, setDeadline] = useState("");
  const [remark, setRemark] = useState("");
  const [jobDescription, setJobDescription] = useState();

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  // const [password, setPassword] = useState("");
  const [role, setRole] = useState("");

  const [requestStatusName, setRequestStatusName] = useState("");
  const [candidateStatusName, setCandidateStatusName] = useState("");

  const [editRequestData, setEditRequestData] = useState(null);
  const [editCandidateData, setEditCandidateData] = useState(null);
  const [editUserData, setEditUserData] = useState(null);
  const [editLocationData, setEditLocationData] = useState(null);
  const [editDomainData, setEditDomainData] = useState(null);
  const [editDesignationData, setEditDesignationData] = useState(null);
  const [editRequestStatusData, setEditRequestStatusData] = useState(null);
  const [editCandidateStatusData, setEditCandidateStatusData] = useState(null);

  const [isAttachResumeModalOpen, setIsAttachResumeModalOpen] = useState(false);

  const [isEditRequestModalOpen, setIsEditRequestModalOpen] = useState(false);
  const [isEditCandidateModalOpen, setIsEditCandidateModalOpen] =
    useState(false);
  const [isEditUserModalOpen, setIsEditUserModalOpen] = useState(false);
  const [isEditLocationModalOpen, setIsEditLocationModalOpen] = useState(false);
  const [isEditDomainModalOpen, setIsEditDomainModalOpen] = useState(false);
  const [isEditDesignationModalOpen, setIsEditDesignationModalOpen] =
    useState(false);
  const [isEditRequestStatusModalOpen, setIsEditRequestStatusModalOpen] =
    useState(false);
  const [isEditCandidateStatusModalOpen, setIsEditCandidateStatusModalOpen] =
    useState(false);

  const [isDeleteLocationModalOpen, setIsDeleteLocationModalOpen] =
    useState(false);
  const [isDeleteDomainModalOpen, setIsDeleteDomainModalOpen] = useState(false);
  const [isDeleteDesignationModalOpen, setIsDeleteDesignationModalOpen] =
    useState(false);
  const [isDeleteRequestStatusModalOpen, setIsDeleteRequestStatusModalOpen] =
    useState(false);
  const [
    isDeleteCandidateStatusModalOpen,
    setIsDeleteCandidateStatusModalOpen,
  ] = useState(false);

  // const [notification, setNotification] = useState("");
  const [isRead] = useState(false);
  const [createdBy] = useState(sessionStorage.getItem("userName"));
  // const [sendTo, setSendTo] = useState("");
  // const [requesterId] = useState(null);
  // const [hrId,setHrId] = useState(null);

  const navigate = useNavigate();

  if (!rows || rows.length === 0) {
    return (
      <p
        className="text-white sticky top-0  bg-gray-300 rounded-lg shadow-md"
        style={{ background: "#0a75bb" }}
      >
        No data available.
      </p>
    );
  }

  function getStatusColor(status) {
    switch (status) {
      case "pending":
        return "#fac3c8";
      case "fulfilled":
        return "#cff1cc";
      case "Submitted":
        return "#d4e8f8";
      case "Resume_Shortlisted":
        return "#f9dd7d";
      case "Resume_Rejected":
        return "#fac3c8";
      case "Interviewed_Selected":
        return "#f9dd7d";
      case "Interviewed_Rejected":
        return "#fac3c8";
      case "Hr_Interviewed_Offered":
        return "#f9dd7d";
      case "Hr_Interviewed_Not_Offered":
        return "#fac3c8";
      case "Joined":
        return "#cff1cc";
      case "Not_Joined":
        return "#fac3c8";
      default:
        return "white";
    }
  }

  function getStatusTextColor(status) {
    switch (status) {
      case "pending":
        return "#64707c";
      case "fulfilled":
        return "#59a858";
      case "Submitted":
        return "#3694dd";
      case "Resume_Shortlisted":
        return "#0c275f";
      case "Resume_Rejected":
        return "#64707c";
      case "Interviewed_Selected":
        return "#0c275f";
      case "Interviewed_Rejected":
        return "#64707c";
      case "Hr_Interviewed_Offered":
        return "#0c275f";
      case "Hr_Interviewed_Not_Offered":
        return "#64707c";
      case "Joined":
        return "#59a858";
      case "Not_Joined":
        return "#64707c";
      default:
        return "white";
    }
  }

  const columns = Object.keys(rows[0]);

  //console.log("clm", { columns });

  function clm(column, columnIndex, row) {
    //console.log("row", row);
    switch (sessionStorage.getItem("userRole")) {
      case "admin":
        switch (column) {
          case "deadline":
            return (
              <>
                <td key={columnIndex}>
                  {new Date(row[column]).toDateString()}
                </td>
              </>
            );

          case "job_description":
            return (
              <>
                <td key={columnIndex}>
                  <Link
                    to={Config.serverURL + "/hr/downloadJD/" + `${row[column]}`}
                    className="tooltip text-blue-600 flex justify-center hover:text-blue-800"
                  >
                    <FaDownload />
                    <span class="tooltiptext">Download</span>
                  </Link>
                </td>
              </>
            );

          case "resume":
            return (
              <>
                <td key={columnIndex}>
                  <Link
                    to={
                      Config.serverURL +
                      "/requester/downloadResume/" +
                      `${row[column]}`
                    }
                    className="tooltip text-blue-600 flex justify-center hover:text-blue-800"
                  >
                    <FaDownload />
                    <span class="tooltiptext">Download</span>
                  </Link>
                </td>
              </>
            );

          default:
            return (
              <>
                <td key={columnIndex}>{row[column]}</td>
              </>
            );
        }

      case "requester":
        switch (column) {
          case "deadline":
            return (
              <>
                <td key={columnIndex}>
                  {new Date(row[column]).toDateString()}
                </td>
              </>
            );

          case "job_description":
            return (
              <>
                <td key={columnIndex}>
                  <Link
                    to={Config.serverURL + "/hr/downloadJD/" + `${row[column]}`}
                    className="tooltip text-blue-600 flex justify-center hover:text-blue-800"
                  >
                    <FaDownload />
                    <span class="tooltiptext">Download</span>
                  </Link>
                </td>
              </>
            );

          case "request_status":
            return (
              <>
                <td key={columnIndex}>
                  <select
                    value={row[column]}
                    onChange={(event) =>
                      handleStatusChange(row, event.target.value)
                    }
                    style={{
                      backgroundColor: getStatusColor(row[column]),
                      color: getStatusTextColor(row[column]),
                    }}
                    className="rounded-lg border border-gray-300 focus:outline-double focus:border-customBlue"
                  >
                    {reqStatuses.map((st, index) => (
                      <option
                        key={st.rq_st_name}
                        value={st.rq_st_name}
                        style={{ backgroundColor: "white", color: "black" }}
                      >
                        {st.rq_st_name}
                      </option>
                    ))}
                  </select>
                </td>
              </>
            );

          case "candidate_status":
            return (
              <>
                <td key={columnIndex}>
                  <select
                    value={row[column]}
                    onChange={(event) =>
                      handleCndStatusChange(row, event.target.value)
                    }
                    style={{
                      backgroundColor: getStatusColor(row[column]),
                      color: getStatusTextColor(row[column]),
                    }}
                    className="rounded-lg border border-gray-300 focus:outline-double focus:border-customBlue"
                  >
                    {cndStatuses.map((st, index) => (
                      <option
                        key={st.cnd_st_name}
                        value={st.cnd_st_name}
                        style={{ backgroundColor: "white", color: "black" }}
                      >
                        {st.cnd_st_name}
                      </option>
                    ))}
                  </select>
                </td>
              </>
            );

          case "resume":
            return (
              <>
                <td key={columnIndex}>
                  <Link
                    to={
                      Config.serverURL +
                      "/requester/downloadResume/" +
                      `${row[column]}`
                    }
                    className="tooltip text-blue-600 flex justify-center hover:text-blue-800"
                  >
                    <FaDownload />
                    <span class="tooltiptext">Download</span>
                  </Link>
                </td>
              </>
            );

          default:
            return (
              <>
                <td key={columnIndex}>{row[column]}</td>
              </>
            );
        }

      case "hr":
        switch (column) {
          case "deadline":
            return (
              <>
                <td key={columnIndex}>
                  {new Date(row[column]).toDateString()}
                </td>
              </>
            );

          case "job_description":
            return (
              <>
                <td key={columnIndex}>
                  <Link
                    to={Config.serverURL + "/hr/downloadJD/" + `${row[column]}`}
                    className="tooltip text-blue-600 flex justify-center hover:text-blue-800"
                  >
                    <FaDownload />
                    <span class="tooltiptext">Download</span>
                  </Link>
                </td>
              </>
            );

          case "request_status":
            return (
              <>
                <td key={columnIndex}>
                  <select
                    value={row[column]}
                    onChange={(event) =>
                      handleStatusChange(row, event.target.value)
                    }
                    style={{
                      backgroundColor: getStatusColor(row[column]),
                      color: getStatusTextColor(row[column]),
                    }}
                    className="rounded-lg border border-gray-300 focus:outline-double focus:border-customBlue"
                  >
                    {reqStatuses.map((st, index) => (
                      <option
                        key={st.rq_st_name}
                        value={st.rq_st_name}
                        style={{ backgroundColor: "white", color: "black" }}
                      >
                        {st.rq_st_name}
                      </option>
                    ))}
                  </select>
                </td>
              </>
            );

          case "candidate_status":
            return (
              <>
                <td key={columnIndex}>
                  <select
                    value={row[column]}
                    onChange={(event) =>
                      handleCndStatusChange(row, event.target.value)
                    }
                    style={{
                      backgroundColor: getStatusColor(row[column]),
                      color: getStatusTextColor(row[column]),
                    }}
                    className="rounded-lg border border-gray-300 focus:outline-double focus:border-customBlue"
                  >
                    {cndStatuses.map((st, index) => (
                      <option
                        key={st.cnd_st_name}
                        value={st.cnd_st_name}
                        style={{ backgroundColor: "white", color: "black" }}
                      >
                        {st.cnd_st_name}
                      </option>
                    ))}
                  </select>
                </td>
              </>
            );

          case "resume":
            return (
              <>
                <td key={columnIndex}>
                  <Link
                    to={
                      Config.serverURL +
                      "/requester/downloadResume/" +
                      `${row[column]}`
                    }
                    className="tooltip text-blue-600 flex justify-center hover:text-blue-800"
                  >
                    <FaDownload />
                    <span class="tooltiptext">Download</span>
                  </Link>
                </td>
              </>
            );

          default:
            return (
              <>
                <td key={columnIndex}>{row[column]}</td>
              </>
            );
        }

      default:
        break;
    }
  }

  const handleStatusChange = (row, newStatus) => {
    const updateStatusUrl =
      Config.serverURL + "/requester/updateRequestStatus/" + row.request_id;
    const body = { requestStatus: newStatus };
    axios
      .put(updateStatusUrl, body)
      .then((response) => {
        const result = response.data;

        const notificationText =
          // "Status of request with id = " +
          // row.request_id +
          // " is changed from " +
          // row.request_status +
          // " to " +
          // newStatus +
          // " by " +
          // createdBy +
          // " at " +
          // new Date().toLocaleString() +
          // ".";
          "Request #" +
          row.request_id +
          ": status changed from '" +
          row.request_status +
          "' to '" +
          newStatus +
          "' by " +
          createdBy;

        if (result.status === "error") {
          toast.error("Error Updating Status!");
        } else {
          axios
            .post(Config.serverURL + "/addNotification", {
              notification: notificationText,
              isRead,
              createdBy,
              sendTo: "AllHr",
              requesterId: row.user_id,
              hrId: null,
              requestId: row.request_id,
            })
            .then((response) => {
              const result = response.data;
              if (result.status === "error") {
                console.log("notification not created");
              } else {
                console.log("notification created");
              }
            })
            .catch((error) => {
              console.log("error creating notification");
              console.log(error);
            });

          toast.success("Successfully updated status!");
          rowLoader();
        }
      })
      .catch((error) => {
        // Handle error
        console.error("Error updating status:", error);
      });
  };

  const handleCndStatusChange = (row, newStatus) => {
    // Send a request to update the status for the specific row by ID
    const updateStatusUrl =
      Config.serverURL + "/requester/updateCandidateStatus/" + row.candidate_id;
    const body = { candidateStatus: newStatus };
    axios
      .put(updateStatusUrl, body)
      .then((response) => {
        const result = response.data;

        const notificationText =
          // "Status of Candidate with id = " +
          // row.candidate_id +
          // " is changed from " +
          // row.candidate_status +
          // " to " +
          // newStatus +
          // " by " +
          // createdBy +
          // " at " +
          // new Date().toLocaleString() +
          // ".";
          "Candidate #" +
          row.candidate_id +
          ": status changed from '" +
          row.candidate_status +
          "' to '" +
          newStatus +
          "' by " +
          createdBy;

        if (result.status === "error") {
          toast.error("Error Updating Status!");
        } else {
          axios
            .post(Config.serverURL + "/addNotification", {
              notification: notificationText,
              isRead,
              createdBy,
              sendTo: "AllHr",
              requesterId: row.requester_id,
              hrId: row.user_id,
              requestId: row.request_id,
            })
            .then((response) => {
              const result = response.data;
              if (result.status === "error") {
                console.log("notification not created");
              } else {
                console.log("notification created");
              }
            })
            .catch((error) => {
              console.log("error creating notification");
              console.log(error);
            });

          toast.success("Successfully updated status!");
          rowLoader();
        }
      })
      .catch((error) => {
        // Handle error
        console.error("Error updating status:", error);
      });
  };

  const AttachResumeModal = (
    e,
    rowRequestId,
    rowRequesterId,
    rowRequesterName
  ) => {
    setRequestId(rowRequestId);
    setRequesterId(rowRequesterId);
    setRequesterName(rowRequesterName);
    setIsAttachResumeModalOpen(true);
  };

  const handleAttachResumeSubmit = (e) => {
    e.preventDefault();

    const formData = new FormData();

    formData.append("candidateName", candidateName);
    formData.append("requestId", requestId);
    formData.append("userId", userId);
    formData.append("candidateResume", resume);
    formData.append("userName", userName);
    formData.append("requesterId", requesterId);
    formData.append("requesterName", requesterName);

    axios
      .post(Config.serverURL + "/hr/provideCandidate", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then((response) => {
        const result = response.data;

        const notificationText =
          // "Resume attached by " +
          // candidateName +
          // " is attached by " +
          // sessionStorage.getItem("userName") +
          // " to Request ID No. " +
          // requestId +
          // " at " +
          // new Date().toLocaleString() +
          // ".";
          "Resume attached by " +
          sessionStorage.getItem("userName") +
          " for Request #" +
          requestId +
          ": " +
          candidateName;

        if (result.status === "error") {
          toast.error("Error while creting request!");
        } else {
          axios
            .post(Config.serverURL + "/addNotification", {
              notification: notificationText,
              isRead,
              createdBy,
              sendTo: requesterName,
              requesterId,
              hrId: sessionStorage.getItem("userId"),
              requestId: requestId,
            })
            .then((response) => {
              const result = response.data;
              if (result.status === "error") {
                console.log("notification not created");
              } else {
                console.log("notification created");
              }
            })
            .catch((error) => {
              console.log("error creating notification");
              console.log(error);
            });

          toast.success("Succefully attached a new resume.");
          navigate("/candidateManagement");
        }
      })
      .catch((error) => {
        console.error("Error submitting form:", error);
      });
  };

  const EditRequestModal = (e, row) => {
    console.log("row", row);
    setEditRequestData(row);
    setIsEditRequestModalOpen(true);
  };

  const handleEditRequestSubmit = (e) => {
    e.preventDefault();

    const formData = new FormData();

    formData.append("location", location);
    formData.append("domain", domain);
    formData.append("designation", designation);
    formData.append("headCount", headCount);
    formData.append("deadline", deadline);
    formData.append("jobDescription", jobDescription);
    formData.append("remark", remark);
    // formData.append("userId", userId);
    // formData.append("userName", userName);

    axios
      .put(
        Config.serverURL +
          "/requester/editRequest/" +
          editRequestData.request_id,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      )
      .then((response) => {
        const result = response.data;
        if (result.status === "error") {
          toast.error("Error while editing request!");
        } else {
          toast.success("Succefully edited request.");
          // navigate("/requestManagement");
          handleModalClose();
          rowLoader();
        }
      })
      .catch((error) => {
        console.error("Error submitting form:", error);
      });
  };

  const EditCandidateModal = (e, row) => {
    console.log("row", row);
    setEditCandidateData(row);
    console.log("qqqqqqqqqqqqq", editCandidateData);
    setIsEditCandidateModalOpen(true);
  };

  const handleEditCandidateSubmit = (e) => {
    e.preventDefault();

    const formData = new FormData();

    formData.append("candidateName", candidateName);
    formData.append("candidateResume", resume);

    axios
      .put(
        Config.serverURL +
          "/hr/editCandidate/" +
          editCandidateData.candidate_id,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      )
      .then((response) => {
        const result = response.data;
        if (result.status === "error") {
          toast.error("Error while editing candidate!");
        } else {
          toast.success("Succefully edited candidate.");
          handleModalClose();
          rowLoader();
        }
      })
      .catch((error) => {
        console.error("Error submitting form:", error);
      });
  };

  const EditUserModal = (e, row) => {
    console.log("row", row);
    setEditUserData(row);
    console.log("aaaaaaaaaaaaaaa", editUserData);
    setIsEditUserModalOpen(true);
  };

  const handleEditUserSubmit = (e) => {
    e.preventDefault();

    axios
      .put(Config.serverURL + "/admin/editUserById/" + editUserData.user_id, {
        name,
        email,
        // password,
        role,
        location,
      })
      .then((response) => {
        const result = response.data;
        if (result.status === "error") {
          toast.error("Error editing user! User email is already registered!");
        } else {
          toast.success("Succefully edited user.");
          handleModalClose();
          rowLoader();
        }
      })
      .catch((error) => {
        toast.error("User Email is already registered!");
        console.log("error");
        console.log("Error submitting form:", error);
      });
  };

  const EditLocationModal = (e, row) => {
    setEditLocationData(row);
    setIsEditLocationModalOpen(true);
  };

  const handleEditLocationSubmit = (e) => {
    e.preventDefault();

    axios
      .put(
        Config.serverURL +
          "/admin/editLocationById/" +
          editLocationData.location_id,
        { location }
      )
      .then((response) => {
        const result = response.data;
        if (result.status === "error") {
          toast.error("Error editing location! Location is already exists!");
        } else {
          toast.success("Succefully edited location!");
          handleModalClose();
          rowLoader();
        }
      });
  };

  const EditDomainModal = (e, row) => {
    setEditDomainData(row);
    setIsEditDomainModalOpen(true);
  };

  const handleEditDomainSubmit = (e) => {
    e.preventDefault();

    axios
      .put(
        Config.serverURL + "/admin/editDomainById/" + editDomainData.domain_id,
        {
          domain,
        }
      )
      .then((response) => {
        const result = response.data;
        if (result.status === "error") {
          toast.error("Error editing domain! Domain is already exists!");
        } else {
          toast.success("Succefully edited domain!");
          handleModalClose();
          rowLoader();
        }
      });
  };

  const EditDesignationModal = (e, row) => {
    setEditDesignationData(row);
    setIsEditDesignationModalOpen(true);
  };

  const handleEditDesignationSubmit = (e) => {
    e.preventDefault();

    axios
      .put(
        Config.serverURL +
          "/admin/editDesignationById/" +
          editDesignationData.designation_id,
        { designation }
      )
      .then((response) => {
        const result = response.data;
        if (result.status === "error") {
          toast.error(
            "Error editing designation! Designation is already exists!"
          );
        } else {
          toast.success("Succefully edited designation!");
          handleModalClose();
          rowLoader();
        }
      });
  };

  const EditRequestStatusModal = (e, row) => {
    setEditRequestStatusData(row);
    setIsEditRequestStatusModalOpen(true);
  };

  const handleEditRequestStatusSubmit = (e) => {
    e.preventDefault();

    axios
      .put(
        Config.serverURL +
          "/admin/editRequestStatusById/" +
          editRequestStatusData.rq_st_id,
        { requestStatusName }
      )
      .then((response) => {
        const result = response.data;
        if (result.status === "error") {
          toast.error(
            "Error editing request status! Request status is already exists!"
          );
        } else {
          toast.success("Succefully edited request status!");
          handleModalClose();
          rowLoader();
        }
      });
  };

  const EditCandidateStatusModal = (e, row) => {
    setEditCandidateStatusData(row);
    setIsEditCandidateStatusModalOpen(true);
  };

  const handleEditCandidateStatusSubmit = (e) => {
    e.preventDefault();

    axios
      .put(
        Config.serverURL +
          "/admin/editCandidateStatusById/" +
          editCandidateStatusData.cnd_st_id,
        { candidateStatusName }
      )
      .then((response) => {
        const result = response.data;
        if (result.status === "error") {
          toast.error(
            "Error editing candidate status! Candidate status is already exists!"
          );
        } else {
          toast.success("Succefully edited candidate status!");
          handleModalClose();
          rowLoader();
        }
      });
  };

  const DeleteLocationModal = (e, row) => {
    setEditLocationData(row);
    setIsDeleteLocationModalOpen(true);
  };

  const handleDeleteLocationSubmit = (e) => {
    axios
      .delete(
        Config.serverURL +
          "/admin/deleteLocationById/" +
          editLocationData.location_id
      )
      .then((response) => {
        const result = response.data;
        if (result.status === "error") {
          toast.error("Error deleting location!");
        } else {
          toast.success(
            "Succefully deleted" + editLocationData.location_name + "location!"
          );
          handleModalClose();
          rowLoader();
        }
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });
  };

  const DeleteDomainModal = (e, row) => {
    setEditDomainData(row);
    setIsDeleteDomainModalOpen(true);
  };

  const handleDeleteDomainSubmit = (e) => {
    axios
      .delete(
        Config.serverURL + "/admin/deleteDomainById/" + editDomainData.domain_id
      )
      .then((response) => {
        const result = response.data;
        if (result.status === "error") {
          toast.error("Error deleting domain!");
        } else {
          toast.success(
            "Succefully deleted" + editDomainData.domain_name + "domain!"
          );
          handleModalClose();
          rowLoader();
        }
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });
  };

  const DeleteDesignationModal = (e, row) => {
    setEditDesignationData(row);
    setIsDeleteDesignationModalOpen(true);
  };

  const handleDeleteDesignationSubmit = (e) => {
    axios
      .delete(
        Config.serverURL +
          "/admin/deleteDesignationById/" +
          editDesignationData.designation_id
      )
      .then((response) => {
        const result = response.data;
        if (result.status === "error") {
          toast.error("Error deleting designation!");
        } else {
          toast.success(
            "Succefully deleted" +
              editDesignationData.designation_name +
              "designation!"
          );
          handleModalClose();
          rowLoader();
        }
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });
  };

  const DeleteRequestStatusModal = (e, row) => {
    setEditRequestStatusData(row);
    setIsDeleteRequestStatusModalOpen(true);
  };

  const handleDeleteRequestStatusSubmit = (e) => {
    axios
      .delete(
        Config.serverURL +
          "/admin/deleteRequestStatusById/" +
          editRequestStatusData.rq_st_id
      )
      .then((response) => {
        const result = response.data;
        if (result.status === "error") {
          toast.error("Error deleting request status!");
        } else {
          toast.success(
            "Succefully deleted" +
              editRequestStatusData.rq_st_name +
              "request status!"
          );
          handleModalClose();
          rowLoader();
        }
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });
  };

  const DeleteCandidateStatusModal = (e, row) => {
    setEditCandidateStatusData(row);
    setIsDeleteCandidateStatusModalOpen(true);
  };

  const handleDeleteCandidateStatusSubmit = (e) => {
    axios
      .delete(
        Config.serverURL +
          "/admin/deleteCandidateStatusById/" +
          editCandidateStatusData.cnd_st_id
      )
      .then((response) => {
        const result = response.data;
        if (result.status === "error") {
          toast.error("Error deleting candidate status!");
        } else {
          toast.success(
            "Succefully deleted" +
              editCandidateStatusData.cnd_st_name +
              "candidate status!"
          );
          handleModalClose();
          rowLoader();
        }
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });
  };

  const handleModalClose = () => {
    setIsAttachResumeModalOpen(false);
    setIsEditRequestModalOpen(false);
    setIsEditCandidateModalOpen(false);
    setIsEditUserModalOpen(false);
    setIsEditLocationModalOpen(false);
    setIsEditDomainModalOpen(false);
    setIsEditDesignationModalOpen(false);
    setIsEditRequestStatusModalOpen(false);
    setIsEditCandidateStatusModalOpen(false);
    setIsDeleteLocationModalOpen(false);
    setIsDeleteDomainModalOpen(false);
    setIsDeleteDesignationModalOpen(false);
    setIsDeleteRequestStatusModalOpen(false);
    setIsDeleteCandidateStatusModalOpen(false);
  };

  function actionColumn(row) {
    switch (sessionStorage.getItem("userRole")) {
      case "admin":
        if (title === "User Details") {
          return (
            <td>
              <button
                className="tooltip"
                onClick={(e) => EditUserModal(e, row)}
              >
                <GrEdit />
                <span class="tooltiptext">Edit User</span>
              </button>
            </td>
          );
        } else if (title === "Location Details") {
          return (
            <td>
              <button
                className="tooltip mr-5"
                onClick={(e) => EditLocationModal(e, row)}
              >
                <GrEdit />
                <span class="tooltiptext">Edit Location</span>
              </button>
              <button
                className="tooltip"
                onClick={(e) => DeleteLocationModal(e, row)}
              >
                <MdDeleteSweep />
                <span class="tooltiptext">Delete Location</span>
              </button>
            </td>
          );
        } else if (title === "Domain Details") {
          return (
            <td>
              <button
                className="tooltip mr-5"
                onClick={(e) => EditDomainModal(e, row)}
              >
                <GrEdit />
                <span class="tooltiptext">Edit Domain</span>
              </button>
              <button
                className="tooltip"
                onClick={(e) => DeleteDomainModal(e, row)}
              >
                <MdDeleteSweep />
                <span class="tooltiptext">Delete Domain</span>
              </button>
            </td>
          );
        } else if (title === "Position Details") {
          return (
            <td>
              <button
                className="tooltip mr-5"
                onClick={(e) => EditDesignationModal(e, row)}
              >
                <GrEdit />
                <span class="tooltiptext">Edit Position</span>
              </button>
              <button
                className="tooltip"
                onClick={(e) => DeleteDesignationModal(e, row)}
              >
                <MdDeleteSweep />
                <span class="tooltiptext">Delete Position</span>
              </button>
            </td>
          );
        } else if (title === "Request Status Details") {
          return (
            <td>
              <button
                className="tooltip mr-5"
                onClick={(e) => EditRequestStatusModal(e, row)}
              >
                <GrEdit />
                <span class="tooltiptext">Edit Request Status</span>
              </button>
              <button
                className="tooltip"
                onClick={(e) => DeleteRequestStatusModal(e, row)}
              >
                <MdDeleteSweep />
                <span class="tooltiptext">Delete Request Status</span>
              </button>
            </td>
          );
        } else if (title === "Candidate Status Details") {
          return (
            <td>
              <button
                className="tooltip mr-5"
                onClick={(e) => EditCandidateStatusModal(e, row)}
              >
                <GrEdit />
                <span class="tooltiptext">Edit Candidate Status</span>
              </button>
              <button
                className="tooltip"
                onClick={(e) => DeleteCandidateStatusModal(e, row)}
              >
                <MdDeleteSweep />
                <span class="tooltiptext">Delete Candidate Status</span>
              </button>
            </td>
          );
        } else if (title === "Request Details") {
          return (
            <td>
              <button className="tooltip">
                <BsDashLg />
                <span class="tooltiptext">No Action</span>
              </button>
            </td>
          );
        } else if (title === "Candidate Details") {
          return (
            <td>
              <button className="tooltip">
                <BsDashLg />
                <span class="tooltiptext">No Action</span>
              </button>
            </td>
          );
        }
        break;

      case "requester":
        if (title === "Request Details") {
          return (
            <td>
              <button
                className="tooltip"
                onClick={(e) => EditRequestModal(e, row)}
              >
                <GrEdit />
                <span class="tooltiptext">Edit Request</span>
              </button>
            </td>
          );
        } else if (title === "Candidate Details") {
          return (
            <td>
              <button className="tooltip">
                <BsDashLg />
                <span class="tooltiptext">No Action</span>
              </button>
            </td>
          );
        }
        break;

      case "hr":
        if (title === "Request Details") {
          return (
            <td>
              <button
                className="tooltip"
                onClick={(e) =>
                  AttachResumeModal(
                    e,
                    row.request_id,
                    row.user_id,
                    row.user_name
                  )
                }
              >
                <ImAttachment />
                <span class="tooltiptext">Attach Resume</span>
              </button>
            </td>
          );
        } else if (title === "Candidate Details") {
          return (
            <td>
              <button
                className="tooltip"
                onClick={(e) => EditCandidateModal(e, row)}
              >
                <GrEdit />
                <span class="tooltiptext">Edit Candidate</span>
              </button>
            </td>
          );
        }
        break;

      default:
        break;
    }
  }

  return (
    <>
      <Modal
        isOpen={
          isAttachResumeModalOpen ||
          isEditRequestModalOpen ||
          isEditCandidateModalOpen ||
          isEditUserModalOpen ||
          isEditLocationModalOpen ||
          isEditDomainModalOpen ||
          isEditDesignationModalOpen ||
          isEditRequestStatusModalOpen ||
          isEditCandidateStatusModalOpen ||
          isDeleteLocationModalOpen ||
          isDeleteDomainModalOpen ||
          isDeleteDesignationModalOpen ||
          isDeleteRequestStatusModalOpen ||
          isDeleteCandidateStatusModalOpen
        }
        onRequestClose={handleModalClose}
        className="modal-content"
        overlayClassName="modal-overlay"
      >
        {isAttachResumeModalOpen && (
          <form onSubmit={handleAttachResumeSubmit}>
            <div className="mb-4">
              <h1 className="block text-black text-center text-lg font-semibold mb-2">
                Attach Resume
              </h1>
            </div>
            <div className="mb-2">
              <label className="block text-left text-black font-semibold mb-2">
                Candidate Name:
              </label>
              <input
                type="text"
                value={candidateName}
                onChange={(event) => {
                  setCandidateName(event.target.value);
                }}
                required
                className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
              />
            </div>
            <div className="mb-4">
              <label className="block text-left text-black font-semibold mb-2">
                Resume:
              </label>
              <input
                type="file"
                name="resume"
                onChange={(event) => {
                  setResume(event.target.files[0]);
                }}
                required
                className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
              />
            </div>
            <button
              type="submit"
              className="w-full bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
              // onClick={(e) => handleSubmit(e)}
            >
              Attach Resume
            </button>
          </form>
        )}

        {isEditRequestModalOpen && editRequestData && (
          <form onSubmit={handleEditRequestSubmit}>
            <div className="mb-4">
              <h1 className="block text-black text-center text-lg font-semibold mb-2">
                Edit Request
              </h1>
            </div>
            <div className="flex mb-4">
              <div className="flex-col">
                <label className="block text-left text-black font-semibold mb-2">
                  Location:
                </label>
                <select
                  name="location"
                  value={editRequestData.location}
                  onChange={(event) => {
                    setLocation(event.target.value);
                    setEditRequestData({
                      ...editRequestData,
                      location: event.target.value,
                    });
                  }}
                  className="w-72 px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
                >
                  <option value="">Select Location</option>
                  {locations.map((loc, index) => (
                    <option key={loc.location_name} value={loc.location_name}>
                      {loc.location_name}
                    </option>
                  ))}
                </select>
              </div>
              <div className="flex-col ml-4">
                <label className="block text-left text-black font-semibold mb-2">
                  Domain:
                </label>
                <select
                  name="domain"
                  value={editRequestData.domain}
                  onChange={(event) => {
                    setDomain(event.target.value);
                    setEditRequestData({
                      ...editRequestData,
                      domain: event.target.value,
                    });
                  }}
                  className="w-72 px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
                >
                  <option value="">Select Domain</option>
                  {domains.map((dom, index) => (
                    <option key={dom.domain_name} value={dom.domain_name}>
                      {dom.domain_name}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div className="flex mb-4">
              <div className="flex-col">
                <label className="block text-left text-black font-semibold mb-2">
                  Designation:
                </label>
                <select
                  name="designation"
                  value={editRequestData.designation}
                  onChange={(event) => {
                    setDesignation(event.target.value);
                    setEditRequestData({
                      ...editRequestData,
                      designation: event.target.value,
                    });
                  }}
                  className="w-48 px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
                >
                  <option value="">Select Designation</option>
                  {designations.map((des, index) => (
                    <option
                      key={des.designation_name}
                      value={des.designation_name}
                    >
                      {des.designation_name}
                    </option>
                  ))}
                </select>
              </div>
              <div className="flex-col ml-4">
                <label className="block text-left text-black font-semibold mb-2">
                  Head Count:
                </label>
                <input
                  type="text"
                  name="headCount"
                  value={editRequestData.head_count}
                  onChange={(event) => {
                    setHeadCount(event.target.value);
                    setEditRequestData({
                      ...editRequestData,
                      head_count: event.target.value,
                    });
                  }}
                  className="w-48 px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
                />
              </div>
              <div className="flex-col ml-4">
                <label className="block text-left text-black font-semibold mb-2">
                  Deadline:
                </label>
                <input
                  type="date"
                  name="deadline"
                  // value={editRequestData.deadline}
                  value={
                    editRequestData.deadline
                      ? new Date(editRequestData.deadline).toLocaleDateString(
                          "en-CA"
                        ) // Change 'en-CA' to the desired locale
                      : ""
                  }
                  onChange={(event) => {
                    setDeadline(event.target.value);
                    setEditRequestData({
                      ...editRequestData,
                      deadline: event.target.value,
                    });
                  }}
                  className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
                />
              </div>
            </div>
            <div className="mb-4">
              <label className="block text-left text-black font-semibold mb-2">
                Job Description:
              </label>
              <input
                type="file"
                name="jobDescription"
                // value={editRequestData.job_description}
                onChange={(event) => {
                  setJobDescription(event.target.files[0]);
                }}
                className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
              />
            </div>
            <div className="mb-4">
              <label className="block text-left text-black font-semibold mb-2">
                Remark:
              </label>
              <input
                type="text"
                name="remark"
                value={editRequestData.remark}
                onChange={(event) => {
                  setRemark(event.target.value);
                  setEditRequestData({
                    ...editRequestData,
                    remark: event.target.value,
                  });
                }}
                className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
              />
            </div>
            <button
              type="submit"
              // onClick={(e) => handleEditRequestSubmit(e)}
              className="w-full bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
            >
              Edit Request
            </button>
          </form>
        )}

        {isEditCandidateModalOpen && editCandidateData && (
          <form onSubmit={handleEditCandidateSubmit}>
            <div className="mb-4">
              <h1 className="block text-black text-center text-lg font-semibold mb-2">
                Edit Candidate
              </h1>
            </div>
            <div className="mb-2">
              <label className="block text-left text-black font-semibold mb-2">
                Candidate Name:
              </label>
              <input
                type="text"
                value={editCandidateData.candidate_name}
                onChange={(event) => {
                  setCandidateName(event.target.value);
                  setEditCandidateData({
                    ...editCandidateData,
                    candidate_name: event.target.value,
                  });
                }}
                className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
              />
            </div>
            <div className="mb-4">
              <label className="block text-left text-black font-semibold mb-2">
                Resume:
              </label>
              <input
                type="file"
                name="resume"
                onChange={(event) => {
                  setResume(event.target.files[0]);
                }}
                className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
              />
            </div>
            <button
              type="submit"
              className="w-full bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
            >
              Edit Candidate
            </button>
          </form>
        )}

        {isEditUserModalOpen && editUserData && (
          <form onSubmit={handleEditUserSubmit}>
            <div className="mb-4">
              <h1 className="block text-black text-center text-lg font-semibold mb-2">
                Edit User
              </h1>
            </div>
            <div className="mb-2">
              <label className="block text-left text-black font-semibold mb-2">
                User Name:
              </label>
              <input
                type="text"
                value={editUserData.user_name}
                onChange={(event) => {
                  setName(event.target.value);
                  setEditUserData({
                    ...editUserData,
                    user_name: event.target.value,
                  });
                }}
                className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
              />
            </div>
            <div className="mb-2">
              <label
                htmlFor="email"
                className="block text-left text-black font-semibold mb-2"
              >
                Email:
              </label>
              <input
                type="email"
                id="email"
                value={editUserData.user_email}
                onChange={(event) => {
                  setEmail(event.target.value);
                  setEditUserData({
                    ...editUserData,
                    user_email: event.target.value,
                  });
                }}
                className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
              />
            </div>
            {/* <div className="mb-2">
              <label
                htmlFor="password"
                className="block text-left text-black font-semibold mb-2"
              >
                Password:
              </label>
              <input
                type="password"
                id="password"
                value={editUserData.user_password}
                onChange={(event) => {
                  setPassword(event.target.value)
                  setEditUserData({
                    ...editUserData,
                    user_password: event.target.value,
                  });
                }}
                className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
              />
            </div> */}
            <div className="mb-2">
              <label className="block text-left text-black font-semibold mb-2">
                Location:
              </label>
              <select
                name="location"
                value={editUserData.user_location}
                onChange={(event) => {
                  setLocation(event.target.value);
                  setEditUserData({
                    ...editUserData,
                    user_location: event.target.value,
                  });
                }}
                className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
              >
                <option value="">Select Location</option>
                {locations.map((location, index) => (
                  <option
                    key={location.location_name}
                    value={location.location_name}
                  >
                    {location.location_name}
                  </option>
                ))}
              </select>
            </div>
            <div className="mb-2">
              <label className="block text-left text-black font-semibold mb-2">
                Role:
              </label>
              <select
                name="role"
                value={editUserData.user_role}
                onChange={(event) => {
                  setRole(event.target.value);
                  setEditUserData({
                    ...editUserData,
                    user_role: event.target.value,
                  });
                }}
                className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
              >
                <option value="">Select Role</option>
                {roles.map((role, index) => (
                  <option key={role.role_name} value={role.role_name}>
                    {role.role_name}
                  </option>
                ))}
              </select>
            </div>
            <button
              type="submit"
              className="w-full bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
            >
              Edit User
            </button>
          </form>
        )}

        {isEditLocationModalOpen && editLocationData && (
          <form onSubmit={handleEditLocationSubmit}>
            <div className="mb-4">
              <h1 className="block text-black text-center text-lg font-semibold mb-2">
                Edit Location
              </h1>
            </div>
            <div className="mb-2">
              <label className="block text-left text-black font-semibold mb-2">
                Location:
              </label>
              <input
                type="text"
                value={editLocationData.location_name}
                onChange={(event) => {
                  setLocation(event.target.value);
                  setEditLocationData({
                    ...editLocationData,
                    location_name: event.target.value,
                  });
                }}
                className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
              />
            </div>
            <button
              type="submit"
              className="w-full bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
            >
              Edit Location
            </button>
          </form>
        )}

        {isEditDomainModalOpen && editDomainData && (
          <form onSubmit={handleEditDomainSubmit}>
            <div className="mb-4">
              <h1 className="block text-black text-center text-lg font-semibold mb-2">
                Edit Domain
              </h1>
            </div>
            <div className="mb-2">
              <label className="block text-left text-black font-semibold mb-2">
                Domain:
              </label>
              <input
                type="text"
                value={editDomainData.domain_name}
                onChange={(event) => {
                  setDomain(event.target.value);
                  setEditDomainData({
                    ...editDomainData,
                    domain_name: event.target.value,
                  });
                }}
                className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
              />
            </div>
            <button
              type="submit"
              className="w-full bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
            >
              Edit Domain
            </button>
          </form>
        )}

        {isEditDesignationModalOpen && editDesignationData && (
          <form onSubmit={handleEditDesignationSubmit}>
            <div className="mb-4">
              <h1 className="block text-black text-center text-lg font-semibold mb-2">
                Edit Position
              </h1>
            </div>
            <div className="mb-2">
              <label className="block text-left text-black font-semibold mb-2">
                Position:
              </label>
              <input
                type="text"
                value={editDesignationData.designation_name}
                onChange={(event) => {
                  setDesignation(event.target.value);
                  setEditDesignationData({
                    ...editDesignationData,
                    designation_name: event.target.value,
                  });
                }}
                className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
              />
            </div>
            <button
              type="submit"
              className="w-full bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
            >
              Edit Position
            </button>
          </form>
        )}

        {isEditRequestStatusModalOpen && editRequestStatusData && (
          <form onSubmit={handleEditRequestStatusSubmit}>
            <div className="mb-4">
              <h1 className="block text-black text-center text-lg font-semibold mb-2">
                Edit Request Status
              </h1>
            </div>
            <div className="mb-2">
              <label className="block text-left text-black font-semibold mb-2">
                Request Status:
              </label>
              <input
                type="text"
                value={editRequestStatusData.rq_st_name}
                onChange={(event) => {
                  setRequestStatusName(event.target.value);
                  setEditRequestStatusData({
                    ...editRequestStatusData,
                    rq_st_name: event.target.value,
                  });
                }}
                className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
              />
            </div>
            <button
              type="submit"
              className="w-full bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
            >
              Edit Request Status
            </button>
          </form>
        )}

        {isEditCandidateStatusModalOpen && editCandidateStatusData && (
          <form onSubmit={handleEditCandidateStatusSubmit}>
            <div className="mb-4">
              <h1 className="block text-black text-center text-lg font-semibold mb-2">
                Edit Candidate Status
              </h1>
            </div>
            <div className="mb-2">
              <label className="block text-left text-black font-semibold mb-2">
                Candidate Status:
              </label>
              <input
                type="text"
                value={editCandidateStatusData.cnd_st_name}
                onChange={(event) => {
                  setCandidateStatusName(event.target.value);
                  setEditCandidateStatusData({
                    ...editCandidateStatusData,
                    cnd_st_name: event.target.value,
                  });
                }}
                className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
              />
            </div>
            <button
              type="submit"
              className="w-full bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
            >
              Edit Candidate Status
            </button>
          </form>
        )}

        {isDeleteLocationModalOpen && editLocationData && (
          <form onSubmit={handleDeleteLocationSubmit}>
            <div className="mb-4">
              <h1 className="block text-black text-center text-lg font-semibold mb-2">
                Are you Sure?
              </h1>
            </div>
            <div>
              <button
                type="submit"
                className="w-32 bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
              >
                Delete
              </button>
              <button
                onClick={handleModalClose}
                className="w-32 ml-2 bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
              >
                Cancel
              </button>
            </div>
          </form>
        )}

        {isDeleteDomainModalOpen && editDomainData && (
          <form onSubmit={handleDeleteDomainSubmit}>
            <div className="mb-4">
              <h1 className="block text-black text-center text-lg font-semibold mb-2">
                Are you Sure?
              </h1>
            </div>
            <div>
              <button
                type="submit"
                className="w-32 bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
              >
                Delete
              </button>
              <button
                onClick={handleModalClose}
                className="w-32 ml-2 bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
              >
                Cancel
              </button>
            </div>
          </form>
        )}

        {isDeleteDesignationModalOpen && editDesignationData && (
          <form onSubmit={handleDeleteDesignationSubmit}>
            <div className="mb-4">
              <h1 className="block text-black text-center text-lg font-semibold mb-2">
                Are you Sure?
              </h1>
            </div>
            <div>
              <button
                type="submit"
                className="w-32 bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
              >
                Delete
              </button>
              <button
                onClick={handleModalClose}
                className="w-32 ml-2 bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
              >
                Cancel
              </button>
            </div>
          </form>
        )}

        {isDeleteRequestStatusModalOpen && editRequestStatusData && (
          <form onSubmit={handleDeleteRequestStatusSubmit}>
            <div className="mb-4">
              <h1 className="block text-black text-center text-lg font-semibold mb-2">
                Are you Sure?
              </h1>
            </div>
            <div>
              <button
                type="submit"
                className="w-32 bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
              >
                Delete
              </button>
              <button
                onClick={handleModalClose}
                className="w-32 ml-2 bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
              >
                Cancel
              </button>
            </div>
          </form>
        )}

        {isDeleteCandidateStatusModalOpen && editCandidateStatusData && (
          <form onSubmit={handleDeleteCandidateStatusSubmit}>
            <div className="mb-4">
              <h1 className="block text-black text-center text-lg font-semibold mb-2">
                Are you Sure?
              </h1>
            </div>
            <div>
              <button
                type="submit"
                className="w-32 bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
              >
                Delete
              </button>
              <button
                onClick={handleModalClose}
                className="w-32 ml-2 bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
              >
                Cancel
              </button>
            </div>
          </form>
        )}
      </Modal>
      <table className="min-w-full border-separate">
        <thead className="text-white sticky top-0 z-10 bg-customBlue">
          <tr>
            <th colSpan={columns.length + 1}>{title}</th>
          </tr>
          <tr>
            {columnsTitles.map((columnTitle, index) => (
              <th key={index}>{columnTitle}</th>
            ))}
            <th>Action</th>
          </tr>
        </thead>
        <tbody className="text-black" style={{ background: "#e7eef3" }}>
          {rows.map((row, rowIndex) => (
            <tr key={rowIndex}>
              {columns.map((column, columnIndex) =>
                clm(column, columnIndex, row)
              )}
              {actionColumn(row)}
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};

export default DynamicTable;
