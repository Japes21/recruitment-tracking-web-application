import React, { useEffect, useState } from "react";
import { MdNotificationsActive } from "react-icons/md";
import { Link } from "react-router-dom";
import { ImSwitch } from "react-icons/im";
import avatar from "../images/Profile_image.png";
import axios from "axios";
import Config from "../config";
import { RxCross1 } from "react-icons/rx";
import { MdOutlineCancel } from "react-icons/md";
import { format } from "date-fns";

const Navbar = () => {
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);
  const [countOfUnreadNotification, setCountOfUnreadNotification] = useState(0);
  const [
    countOfUnreadNotificationForRequester,
    setCountOfUnreadNotificationForRequester,
  ] = useState(0);
  const [isNotificationSidebarOpen, setIsNotificationSidebarOpen] =
    useState(false);
  const [notification, setNotification] = useState([]);

  useEffect(() => {
    let interval = setInterval(async () => {
      if (sessionStorage.getItem("userRole") === "requester") {
        await axios
          .get(
            Config.serverURL +
              "/getCountOfUnreadNotificationsForRequester/" +
              sessionStorage.getItem("userId")
          )
          .then((response) => {
            let result = response.data;
            console.log("count", result[0].unread_notifications);
            setCountOfUnreadNotificationForRequester(
              result[0].unread_notifications
            );
          })
          .catch((error) => {
            console.log("error");
            console.log(error);
          });
      } else {
        await axios
          .get(Config.serverURL + "/getCountOfUnreadNotifications")
          .then((response) => {
            let result = response.data;
            console.log("count", result[0].unread_notifications);
            setCountOfUnreadNotification(result[0].unread_notifications);
          })
          .catch((error) => {
            console.log("error");
            console.log(error);
          });
      }
    }, 2000);
    return () => {
      clearInterval(interval);
    };
  }, []);

  // useEffect(() => {
  //   let interval = setInterval(async () => {
  //     await axios
  //       .get(
  //         Config.serverURL +
  //           "/getCountOfUnreadNotificationsForRequester/" +
  //           sessionStorage.getItem("userId")
  //       )
  //       .then((response) => {
  //         let result = response.data;
  //         console.log("count", result[0].unread_notifications);
  //         setCountOfUnreadNotification(result[0].unread_notifications);
  //       })
  //       .catch((error) => {
  //         console.log("error");
  //         console.log(error);
  //       });
  //   }, 2000);
  //   return () => {
  //     clearInterval(interval);
  //   };
  // }, []);

  const handleDropdownToggle = () => {
    setIsDropdownOpen(!isDropdownOpen);
  };

  const handleNotificationSidebarToggle = () => {
    setIsNotificationSidebarOpen(!isNotificationSidebarOpen);
  };

  const Logout = () => {
    sessionStorage.clear();
  };

  const getNotifications = () => {
    switch (sessionStorage.getItem("userRole").toLowerCase()) {
      case "admin":
        axios
          .get(Config.serverURL + "/getAllNotifications")
          .then((response) => {
            let result = response.data;
            console.log("nnnnnnnnnnnnn", result);
            setNotification(result);
          })
          .catch((error) => {
            console.log("error");
            console.log(error);
          });
        break;

      case "requester":
        axios
          .get(
            Config.serverURL +
              "/getAllNotificationsForRequester/" +
              sessionStorage.getItem("userId")
          )
          .then((response) => {
            let result = response.data;
            console.log("nnnnnnnnnnnnn", result);
            setNotification(result);
          })
          .catch((error) => {
            console.log("error");
            console.log(error);
          });
        break;

      case "hr":
        axios
          .get(Config.serverURL + "/getAllNotifications")
          .then((response) => {
            let result = response.data;
            setNotification(result);
          })
          .catch((error) => {
            console.log("error");
            console.log(error);
          });
        break;

      default:
        break;
    }
  };

  return (
    <nav className=" w-full text-white py-2 px-6">
      <div className="flex justify-end items-center">
        <div className="relative">
          <div className="flex space-x-1 items-center">
            <button
              className="tooltip block text-3xl text-gray-600 hover:text-black transition duration-200"
              onClick={(e) => {
                handleNotificationSidebarToggle();
                getNotifications();
              }}
            >
              <MdNotificationsActive />
              <div class="absolute inline-flex items-center justify-center w-6 h-6 text-xs font-bold text-white bg-red-500 border-2 border-white rounded-full -top-2 -right-2 dark:border-gray-900">
                {sessionStorage.getItem("userRole") === "requester" ? (
                  <>{countOfUnreadNotificationForRequester}</>
                ) : (
                  <>{countOfUnreadNotification}</>
                )}
              </div>
              <span class="tooltiptext text-xs">Notifications</span>
            </button>
            {isNotificationSidebarOpen && (
              <div className="fixed top-14 right-0 h-full w-96 bg-white z-50 shadow-lg transition-transform transform translate-x-0">
                <div
                  onClick={handleNotificationSidebarToggle}
                  className="absolute top-0 right-0 p-3 text-xl rounded-full cursor-pointer text-gray-600 hover:text-black transition duration-200"
                >
                  <MdOutlineCancel />
                </div>
                <div className="text-xl  text-black text-left p-3">
                  Notifications
                </div>
                <div className="overflow-auto h-[28rem]">
                  {notification.map((item, index) => (
                    <div
                      key={index}
                      className="flex items-center border border-b-1 border-slate-300 p-3"
                    >
                      <div>
                        <p className=" text-black text-left">
                          {item.notification}
                        </p>
                        <p className="text-gray-500 text-sm dark:text-gray-400">
                          {format(new Date(item.created_at), "PPP")}
                        </p>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            )}

            <button
              className=" text-gray-600 hover:text-black text-sm  font-semibold px-4 py-2 rounded-full flex items-center focus:outline-none"
              onClick={handleDropdownToggle}
            >
              <div className="mr-1">
                <img
                  className="rounded-full h-8 w-8"
                  src={avatar}
                  alt="user-profile"
                />
              </div>
              <span>
                Hi,&nbsp;
                <span className="uppercase">
                  {sessionStorage.getItem("userName")}
                </span>
              </span>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className={`ml-1 font-medium h-4 w-4 ${
                  isDropdownOpen ? "transform rotate-180" : ""
                }`}
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={4}
                  d="M19 9l-7 7-7-7"
                />
              </svg>
            </button>
            <Link
              to="/"
              onClick={Logout}
              className="tooltip block text-md  text-gray-600 hover:text-black transition duration-200"
            >
              <ImSwitch />
              <span class="tooltiptext text-xs">Log Out</span>
            </Link>
          </div>
          {isDropdownOpen && (
            <div className="text-justify absolute right-0 mt-2 p-4 w-72 bg-white rounded-lg shadow-lg z-50">
              {/* <p className="text-black  font-semibold text-lg dark:text-gray-200">
                User Profile
              </p> */}

              <div className="flex gap-2 items-center mt-3 border-color border-b-1 pb-2">
                <img
                  className="rounded-full h-24 w-24"
                  src={avatar}
                  alt="user-profile"
                />

                <div className="flex flex-col">
                  <span className=" text-black font-semibold text-xl dark:text-gray-200">
                    {sessionStorage.getItem("userName")}
                  </span>

                  <span className=" uppercase text-gray-500 text-sm dark:text-gray-400">
                    {sessionStorage.getItem("userRole")}
                  </span>

                  <span className=" text-gray-500 text-sm font-semibold dark:text-gray-400">
                    {sessionStorage.getItem("userEmail")}
                  </span>

                  <span className=" text-gray-800 text-sm font-semibold">
                    {sessionStorage.getItem("userId")}
                  </span>
                </div>
              </div>
            </div>
          )}
        </div>
        {/* <button className="bg-gray-400 text-white font-semibold px-4 py-2 rounded-full">
          Logout
        </button> */}
      </div>
    </nav>
  );
};

export default Navbar;
