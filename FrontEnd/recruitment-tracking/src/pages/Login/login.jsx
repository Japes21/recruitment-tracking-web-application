import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import axios from "axios";

import Config from "../../config";
import logo1 from "../../images/logo1.png";
import HR from "../../images/HR.jpg";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();

    axios
      .post(Config.serverURL + "/login", {
        email,
        password,
      })
      .then((response) => {
        const result = response.data;

        if (result.status === "error") {
          toast.error("Invalid email or Password");
        } else {
          console.log(result);
          sessionStorage.setItem("userId", result.data.id);
          sessionStorage.setItem("userName", result.data.name);
          sessionStorage.setItem("userEmail", result.data.email);
          sessionStorage.setItem("userRole", result.data.role);
          sessionStorage.setItem("isLoggedIn", true);

          toast.success("Welcome to Allygrow!");
          // if (sessionStorage.getItem("userRole") === "admin") {
          //   navigate("/adminHome");
          // } else if (sessionStorage.getItem("userRole") === "requester") {
          //   navigate("/requesterHome");
          // } else {
          //   navigate("/hrHome");
          // }
          navigate("/homePage");
        }
      })
      .catch((error) => {
        console.log("error");
        toast.error("Invalid email or password!");
        console.log(error);
      });
  };
  return (
    <div
      className="min-h-screen flex items-center justify-center"
      style={{ backgroundImage: `url(${HR})` }}
    >
      <div className="max-w-md w-full p-6 bg-white bg-opacity-90 rounded-3xl shadow-md">
        <div className="flex justify-center mb-4">
          <img src={logo1} />
        </div>

        <h1 className="text-3xl text-center font-semibold mb-6">Login</h1>
        <form onSubmit={handleSubmit}>
          <div className="mb-4">
            <label
              htmlFor="email"
              className="block text-gray-700 font-semibold mb-2"
            >
              Email:
            </label>
            <input
              type="email"
              id="email"
              value={email}
              onChange={handleEmailChange}
              required
              className="w-full px-4 py-2 rounded border border-gray-300 focus:outline-none focus:border-indigo-500"
            />
          </div>
          <div className="mb-4">
            <label
              htmlFor="password"
              className="block text-gray-700 font-semibold mb-2"
            >
              Password:
            </label>
            <input
              type="password"
              id="password"
              value={password}
              onChange={handlePasswordChange}
              required
              className="w-full px-4 py-2 rounded border border-gray-300 focus:outline-none focus:border-indigo-500"
            />
          </div>
          <button
            type="submit"
            className="w-full bg-indigo-500 text-white font-semibold py-2 px-4 rounded hover:bg-indigo-600 transition duration-200"
          >
            Login
          </button>
        </form>
      </div>
    </div>
  );
};

export default Login;
