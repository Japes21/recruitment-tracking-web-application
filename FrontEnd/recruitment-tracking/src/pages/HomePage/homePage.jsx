import React, { useEffect, useState } from "react";
import Chart from "react-apexcharts";
import axios from "axios";
import Config from "./../../config";

const HomePage = () => {
  const [role] = useState(sessionStorage.getItem("userRole"));

  let result;
  let pendingRequests;
  let fulfilledRequests;

  const [totalCount, setTotalCount] = useState(0);
  const [pendingCount, setPendingCount] = useState(0);
  const [fulfilledCount, setFulfilledCount] = useState(0);
  const [createdCount, setCreatedCount] = useState(0);
  const [pendingCountByUser, setPendingCountByUser] = useState(0);
  const [fulfilledCountByUser, setFulfilledCountByUser] = useState(0);

  useEffect(() => {
    axios
      .get(Config.serverURL + "/admin/getCountOfAllRequests")
      .then((response) => {
        result = response.data;
        // console.log(result);
        // console.log(result[0].totalCount);
        setTotalCount(result[0].totalCount);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });
  }, []);

  useEffect(() => {
    axios
      .get(Config.serverURL + "/admin/getCountOfPendingRequests")
      .then((response) => {
        result = response.data;
        setPendingCount(result[0].pendingCount);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });
  }, []);

  useEffect(() => {
    axios
      .get(Config.serverURL + "/admin/getCountOfFulfilledRequests")
      .then((response) => {
        result = response.data;
        setFulfilledCount(result[0].fulfilledCount);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });
  }, []);

  useEffect(() => {
    axios
      .get(
        Config.serverURL +
          "/requester/getCountOfAllRequestsByUserId/" +
          sessionStorage.getItem("userId")
      )
      .then((response) => {
        result = response.data;
        setCreatedCount(result[0].createdCount);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });
  }, []);

  useEffect(() => {
    axios
      .get(
        Config.serverURL +
          "/requester/getCountOfPendingRequestsByUserId/" +
          sessionStorage.getItem("userId")
      )
      .then((response) => {
        result = response.data;
        setPendingCountByUser(result[0].pendingCountByUser);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });
  }, []);

  useEffect(() => {
    axios
      .get(
        Config.serverURL +
          "/requester/getCountOfFulfilledRequestsByUserId/" +
          sessionStorage.getItem("userId")
      )
      .then((response) => {
        result = response.data;
        setFulfilledCountByUser(result[0].fulfilledCountByUser);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });
  }, []);

  if (role === "admin" || role === "hr") {
    pendingRequests = pendingCount;
    fulfilledRequests = fulfilledCount;
  } else if (role === "requester") {
    pendingRequests = pendingCountByUser;
    fulfilledRequests = fulfilledCountByUser;
  }

  const getCardItems = () => {
    if (role === "admin" || role === "hr") {
      return [
        {
          title: "Total Requests",
          count: totalCount,
        },
        {
          title: "Pending Requests",
          count: pendingCount,
        },
        {
          title: "Fulfilled Requests",
          count: fulfilledCount,
        },
      ];
    } else if (role === "requester") {
      return [
        {
          title: "Created Requests",
          count: createdCount,
        },
        {
          title: "Pending Requests",
          count: pendingCountByUser,
        },
        {
          title: "Fulfilled Requests",
          count: fulfilledCountByUser,
        },
      ];
    } else {
      return [];
    }
  };

  const cardItems = getCardItems();
  //console.log("cardItems", cardItems);

  return (
    <div className="flex-col">
      <div className="flex justify-center items-center space-x-10 m-4">
        {cardItems.map((item, index) => (
          <div className="bg-gray-100 text-center w-1/3 rounded-lg shadow-lg p-4">
            <ul className="p-4">
              <li key={index}>
                <h4 className="text-xl font-semibold mb-2">{item.count}</h4>
                <h4 className="text-xl font-semibold">{item.title}</h4>
              </li>
            </ul>
          </div>
        ))}
      </div>
      <div className="flex justify-start">
        <Chart
          type="donut"
          height={350}
          width={800}
          series={[fulfilledRequests, pendingRequests]}
          options={{
            labels: ["Fulfilled Requests", "Pending Requests"],
            legend: {
              position: "right",
              fontSize: "15px",
            },
            colors: ["#f5a733", "#0a77bb"],
            plotOptions: {
              pie: {
                startAngle: -90,
                endAngle: 270,
                donut: {
                  labels: {
                    show: true,
                    total: {
                      show: true,
                      fontSize: 15,
                    },
                  },
                },
              },
            },
            fill: {
              type: "gradient",
            },
          }}
        />
      </div>
    </div>
  );
};

export default HomePage;
