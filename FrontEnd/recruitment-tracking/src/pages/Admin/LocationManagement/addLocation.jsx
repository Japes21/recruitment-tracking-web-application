import React, { useState, useEffect } from "react";
import Config from "../../../config";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

const AddLocation = () => {
  const [locationName, setLocationName] = useState("");

  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();

    axios
      .post(Config.serverURL + "/admin/addLocation", {
        locationName,
      })
      .then((response) => {
        const result = response.data;
        if (result.status === "error") {
          toast.error("Location is already registered!");
        } else {
          toast.success("Succefully registered a new location.");
          navigate("/locationManagement");
        }
      })
      .catch((error) => {
        toast.error("Location is already registered!");
        console.log("error");
        console.log(error);
      });
  };

  return (
    <div className="flex justify-center items-center mt-10">
      <form onSubmit={handleSubmit}>
        <div className="mb-4">
          <h1 className="block text-black text-lg font-semibold mb-2">
            Add Location
          </h1>
        </div>
        <div className="mb-4">
          <label className="block text-left text-black font-semibold mb-2">
            Name:
          </label>
          <input
            type="text"
            value={locationName}
            onChange={(event) => {
              setLocationName(event.target.value);
            }}
            required
            className="w-96 px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
          />
        </div>
        <button
          type="submit"
          className="w-full bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
        >
          Add Location
        </button>
      </form>
    </div>
  );
};

export default AddLocation;
