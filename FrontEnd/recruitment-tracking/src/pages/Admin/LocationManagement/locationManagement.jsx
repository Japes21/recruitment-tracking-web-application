import React, { useEffect, useState } from "react";
import DynamicTable from "../../../components/dynamicTable";
import axios from "axios";
import Config from "./../../../config";
import { Link } from "react-router-dom";

import DynamicSearchCard from "../../../components/dynamicSearchCard";

const LocationManagement = () => {
  const [locations, setLocations] = useState([]);

  let result;

  useEffect(() => {
    getLocations();
  }, []);

  const getLocations = () => {
    axios.get(Config.serverURL + "/admin/getAllLocations").then((response) => {
      result = response.data;
      console.log(result);

      setLocations(response.data);
    });
  };

  const tableData = locations;
  const title = "Location Details";
  const columnsTitles = ["Location ID", "Location Name"];

  const handleFind = (searchBy, searchValue) => {
    console.log(searchValue);
    console.log({ searchBy });
    if (searchBy === "ID") {
      axios
        .get(Config.serverURL + "/admin/getLocationByLocationId/" + searchValue)
        .then((response) => {
          setLocations(response.data);
        });
    }
  };

  return (
    <div>
      <div className="flex-col justify-center p-10">
        <div className="flex mb-1">
          <Link to={"/addLocation"}>
            <button className="flex text-white font-semibold text-center p-1 rounded-full bg-gray-800 hover:bg-gray-600 active:bg-gray-500 focus:outline-none focus:ring focus:ring-white">
              Add Location
            </button>
          </Link>
          <div className="ml-2">
            <DynamicSearchCard
              search="Location"
              searchBy="ID"
              onFind={handleFind}
            />
          </div>
        </div>
        <div className="max-w-4xl min-w-full overflow-y-scroll overflow-x-scroll max-h-96 bg-gray-300 rounded-lg shadow-md">
          <DynamicTable
            rows={tableData}
            columnsTitles={columnsTitles}
            title={title}
            rowLoader={getLocations}
          />
        </div>
      </div>
    </div>
  );
};

export default LocationManagement;
