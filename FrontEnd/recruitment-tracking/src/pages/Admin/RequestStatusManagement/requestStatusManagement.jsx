import React, { useEffect, useState } from "react";
import DynamicTable from "../../../components/dynamicTable";
import axios from "axios";
import Config from "./../../../config";
import { Link } from "react-router-dom";

const RequestStatusManagement = () => {
  const [requestStatuses, setRequestStatuses] = useState([]);

  let result;

  useEffect(() => {
    getRequestStatuses();
  }, []);

  const getRequestStatuses = () => {
    axios
      .get(Config.serverURL + "/admin/getAllRequestStatus")
      .then((response) => {
        result = response.data;
        console.log(result);

        setRequestStatuses(response.data);
      });
  };

  const tableData = requestStatuses;
  const title = "Request Status Details";
  const columnsTitles = ["Request Status ID", "Request Status Name"];

  return (
    <div>
      <div className="flex-col justify-center p-10">
        <div className="flex">
          <Link to={"/addRequestStatus"}>
            <button className="flex justify-start mb-1 text-white font-semibold text-center p-1 rounded-full bg-gray-800 hover:bg-gray-600 active:bg-gray-500 focus:outline-none focus:ring focus:ring-white">
              Add Request Status
            </button>
          </Link>
        </div>
        <div className="max-w-4xl min-w-full overflow-y-scroll overflow-x-scroll max-h-96 bg-gray-300 rounded-lg shadow-md">
          <DynamicTable
            rows={tableData}
            columnsTitles={columnsTitles}
            title={title}
            rowLoader={getRequestStatuses}
          />
        </div>
      </div>
    </div>
  );
};

export default RequestStatusManagement;
