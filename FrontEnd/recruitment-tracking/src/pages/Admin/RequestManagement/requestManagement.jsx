import React, { useEffect, useState } from "react";
import DynamicTable from "../../../components/dynamicTable";
import axios from "axios";
import Config from "./../../../config";

import DynamicSearchCard from "../../../components/dynamicSearchCard";

const RequestManagement = () => {
  const [requests, setRequests] = useState([]);

  let result;

  useEffect(() => {
    getRequests();
  }, []);

  const getRequests = () => {
    if (
      sessionStorage.getItem("userRole") === "admin" ||
      sessionStorage.getItem("userRole") === "hr"
    ) {
      axios.get(Config.serverURL + "/admin/getAllRequests").then((response) => {
        result = response.data;
        console.log(result);

        setRequests(response.data);
      });
    } else if (sessionStorage.getItem("userRole") === "requester") {
      axios
        .get(
          Config.serverURL +
            "/requester/getAllRequestsByUserId/" +
            sessionStorage.getItem("userId")
        )
        .then((response) => {
          result = response.data;
          console.log(result);

          setRequests(response.data);
        });
    }
  };

  const tableData = requests;
  const title = "Request Details";
  const columnsTitles = [
    "Request ID",
    "Requester ID",
    "Requester Name",
    "Status",
    "Location",
    "Domain",
    "Position",
    "Head Count",
    "Deadline",
    "Remark",
    "Job Description",
  ];

  const handleFind = (searchBy, searchValue) => {
    // console.log(searchValue);
    // console.log({ searchBy });

    if (
      sessionStorage.getItem("userRole") === "admin" ||
      sessionStorage.getItem("userRole") === "hr"
    ) {
      if (searchBy === "ID") {
        axios
          .get(Config.serverURL + "/admin/getRequestByRequestId/" + searchValue)
          .then((response) => {
            setRequests(response.data);
          });
      } else if (searchBy === "Location") {
        //console.log("in else");
        axios
          .get(
            Config.serverURL + "/admin/getAllRequestsByLocation/" + searchValue
          )
          .then((response) => {
            console.log(response.data);
            setRequests(response.data);
          });
      } else if (searchBy === "Domain") {
        //console.log("in else");
        axios
          .get(
            Config.serverURL + "/admin/getAllRequestsByDomain/" + searchValue
          )
          .then((response) => {
            console.log(response.data);
            setRequests(response.data);
          });
      } else if (searchBy === "Position") {
        //console.log("in else");
        axios
          .get(
            Config.serverURL +
              "/admin/getAllRequestsByDesignation/" +
              searchValue
          )
          .then((response) => {
            console.log(response.data);
            setRequests(response.data);
          });
      } else if (searchBy === "Request Status") {
        //console.log("in else");
        axios
          .get(
            Config.serverURL + "/admin/getAllRequestsByStatus/" + searchValue
          )
          .then((response) => {
            console.log(response.data);
            setRequests(response.data);
          });
      } else if (searchBy === "Requester ID") {
        //console.log("in else");
        axios
          .get(
            Config.serverURL +
              "/requester/getAllRequestsByUserId/" +
              searchValue
          )
          .then((response) => {
            console.log(response.data);
            setRequests(response.data);
          });
      }
    } else if (sessionStorage.getItem("userRole") === "requester") {
      if (searchBy === "ID") {
        axios
          .get(
            Config.serverURL +
              "/requester/getRequestByRequestId&UserId/" +
              searchValue +
              "/" +
              sessionStorage.getItem("userId")
          )
          .then((response) => {
            setRequests(response.data);
          });
      } else if (searchBy === "Location") {
        //console.log("in else");
        axios
          .get(
            Config.serverURL +
              "/requester/getAllRequestsByLocation&UserId/" +
              searchValue +
              "/" +
              sessionStorage.getItem("userId")
          )
          .then((response) => {
            console.log(response.data);
            setRequests(response.data);
          });
      } else if (searchBy === "Domain") {
        //console.log("in else");
        axios
          .get(
            Config.serverURL +
              "/requester/getAllRequestsByDomain&UserId/" +
              searchValue +
              "/" +
              sessionStorage.getItem("userId")
          )
          .then((response) => {
            console.log(response.data);
            setRequests(response.data);
          });
      } else if (searchBy === "Position") {
        //console.log("in else");
        axios
          .get(
            Config.serverURL +
              "/requester/getAllRequestsByDesignation&UserId/" +
              searchValue +
              "/" +
              sessionStorage.getItem("userId")
          )
          .then((response) => {
            console.log(response.data);
            setRequests(response.data);
          });
      } else if (searchBy === "Request Status") {
        //console.log("in else");
        axios
          .get(
            Config.serverURL +
              "/requester/getAllRequestsByStatus&UserId/" +
              searchValue +
              "/" +
              sessionStorage.getItem("userId")
          )
          .then((response) => {
            console.log(response.data);
            setRequests(response.data);
          });
      }
    }
  };

  function callCard() {
    switch (sessionStorage.getItem("userRole")) {
      case "requester":
        return <></>;

      default:
        return (
          <DynamicSearchCard
            search="Request"
            searchBy="Requester ID"
            onFind={handleFind}
          />
        );
    }
  }

  return (
    <div>
      <div className="flex-col justify-center p-5">
        <div className="flex mb-1">
          <div>
            <DynamicSearchCard
              search="Request"
              searchBy="ID"
              onFind={handleFind}
            />
          </div>
          <div className="ml-2">
            <DynamicSearchCard
              search="Request"
              searchBy="Location"
              onFind={handleFind}
            />
          </div>
          <div className="ml-2">
            <DynamicSearchCard
              search="Request"
              searchBy="Domain"
              onFind={handleFind}
            />
          </div>
          <div className="ml-2">
            <DynamicSearchCard
              search="Request"
              searchBy="Position"
              onFind={handleFind}
            />
          </div>
          <div className="ml-2">
            <DynamicSearchCard
              search="Request"
              searchBy="Request Status"
              onFind={handleFind}
            />
          </div>
          <div className="ml-2">
            {/* <DynamicSearchCard
              search="Request"
              searchBy="Requester ID"
              onFind={handleFind}
            /> */}
            {callCard()}
          </div>
        </div>
        <div className="max-w-4xl min-w-full overflow-y-scroll overflow-x-scroll max-h-96 bg-gray-300 rounded-lg shadow-md">
          <DynamicTable
            rows={tableData}
            columnsTitles={columnsTitles}
            title={title}
            rowLoader={getRequests}
          />
        </div>
      </div>
    </div>
  );
};

export default RequestManagement;
