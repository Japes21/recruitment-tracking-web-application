import React, { useEffect, useState } from "react";
import DynamicTable from "../../../components/dynamicTable";
import axios from "axios";
import Config from "./../../../config";
import { Link } from "react-router-dom";

import DynamicSearchCard from "../../../components/dynamicSearchCard";

const CandidateManagement = () => {
  const [candidates, setCandidates] = useState([]);

  let result;

  useEffect(() => {
    getCandidates();
  }, []);

  const getCandidates = () => {
    if (
      sessionStorage.getItem("userRole") === "admin" ||
      sessionStorage.getItem("userRole") === "hr"
    ) {
      axios
        .get(Config.serverURL + "/admin/getAllCandidates")
        .then((response) => {
          result = response.data;
          console.log(result);

          setCandidates(response.data);
        });
    } else if (sessionStorage.getItem("userRole") === "requester") {
      axios
        .get(
          Config.serverURL +
            "/requester/getAllCandidatesByRequesterId/" +
            sessionStorage.getItem("userId")
        )
        .then((response) => {
          setCandidates(response.data);
        });
    }
  };

  const tableData = candidates;
  const title = "Candidate Details";
  const columnsTitles = [
    "Candidate ID",
    "Candidate Name",
    "Request ID",
    "Requester Name",
    "HR Name",
    "Resume",
    "Candidate Status",
    "Requester ID",
    "HR ID",
  ];

  const handleFind = (searchBy, searchValue) => {
    if (
      sessionStorage.getItem("userRole") === "admin" ||
      sessionStorage.getItem("userRole") === "hr"
    ) {
      if (searchBy === "ID") {
        axios
          .get(
            Config.serverURL + "/admin/getCandidateByCandidateId/" + searchValue
          )
          .then((response) => {
            setCandidates(response.data);
          });
      } else if (searchBy === "Request ID") {
        //console.log("in else");
        axios
          .get(
            Config.serverURL + "/admin/getCandidatesByRequestId/" + searchValue
          )
          .then((response) => {
            console.log(response.data);
            setCandidates(response.data);
          });
      } else if (searchBy === "Candidate Status") {
        //console.log("in else");
        axios
          .get(
            Config.serverURL +
              "/admin/getCandidatesByCandidateStatus/" +
              searchValue
          )
          .then((response) => {
            console.log(response.data);
            setCandidates(response.data);
          });
      }
    } else if (sessionStorage.getItem("userRole") === "requester") {
      if (searchBy === "ID") {
        axios
          .get(
            Config.serverURL +
              "/requester/getCandidateByCandidateId&RequesterId/" +
              searchValue +
              "/" +
              sessionStorage.getItem("userId")
          )
          .then((response) => {
            setCandidates(response.data);
          });
      } else if (searchBy === "Request ID") {
        //console.log("in else");
        axios
          .get(
            Config.serverURL +
              "/requester/getAllCandidatesByRequestId&RequesterId/" +
              searchValue +
              "/" +
              sessionStorage.getItem("userId")
          )
          .then((response) => {
            console.log(response.data);
            setCandidates(response.data);
          });
      } else if (searchBy === "Candidate Status") {
        //console.log("in else");
        axios
          .get(
            Config.serverURL +
              "/requester/getAllCandidatesByCandidateStatus&RequesterId/" +
              searchValue +
              "/" +
              sessionStorage.getItem("userId")
          )
          .then((response) => {
            console.log(response.data);
            setCandidates(response.data);
          });
      }
    }
  };

  return (
    <div>
      <div className="flex-col justify-center p-10">
        <div className="flex mb-1">
          <div>
            <DynamicSearchCard
              search="Candidate"
              searchBy="ID"
              onFind={handleFind}
            />
          </div>
          <div className="ml-2">
            <DynamicSearchCard
              search="Candidate"
              searchBy="Request ID"
              onFind={handleFind}
            />
          </div>
          <div className="ml-2">
            <DynamicSearchCard
              search="Candidate"
              searchBy="Candidate Status"
              onFind={handleFind}
            />
          </div>
        </div>
        <div className="max-w-4xl min-w-full overflow-y-scroll overflow-x-scroll max-h-96 bg-gray-300 rounded-lg shadow-md">
          <DynamicTable
            rows={tableData}
            columnsTitles={columnsTitles}
            title={title}
            rowLoader={getCandidates}
          />
        </div>
      </div>
    </div>
  );
};

export default CandidateManagement;
