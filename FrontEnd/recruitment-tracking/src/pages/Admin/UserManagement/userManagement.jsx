import React, { useEffect, useState } from "react";
import DynamicTable from "../../../components/dynamicTable";
import axios from "axios";
import Config from "./../../../config";
import { Link } from "react-router-dom";

import DynamicSearchCard from "../../../components/dynamicSearchCard";

const UserManagement = () => {
  const [users, setUsers] = useState([]);

  let result;

  useEffect(() => {
    getUsers();
  }, []);

  const getUsers = () => {
    axios.get(Config.serverURL + "/admin/getAllUsers").then((response) => {
      result = response.data;
      console.log(result);

      setUsers(response.data);
    });
  };

  const tableData = users;
  const title = "User Details";
  const columnsTitles = [
    "User ID",
    "User Name",
    "User Email",
    "User Role",
    "User Location",
  ];

  const handleFind = (searchBy, searchValue) => {
    console.log(searchValue);
    console.log({ searchBy });
    if (searchBy === "ID") {
      axios
        .get(Config.serverURL + "/admin/getUserByUserId/" + searchValue)
        .then((response) => {
          setUsers(response.data);
        });
    } else if (searchBy === "Role") {
      //console.log("in else");
      axios
        .get(Config.serverURL + "/admin/getUserByUserRole/" + searchValue)
        .then((response) => {
          console.log(response.data);
          setUsers(response.data);
        });
    } else if (searchBy === "Location") {
      //console.log("in else");
      axios
        .get(Config.serverURL + "/admin/getUserByUserLocation/" + searchValue)
        .then((response) => {
          console.log(response.data);
          setUsers(response.data);
        });
    }
  };

  return (
    <div>
      <div className="flex-col justify-center p-10">
        <div className="flex mb-1">
          <Link to={"/addUser"}>
            <button className="flex text-white font-semibold text-center p-1 rounded-full bg-gray-800 hover:bg-gray-600 active:bg-gray-500 focus:outline-none focus:ring focus:ring-white">
              Add User
            </button>
          </Link>
          <div className="ml-2">
            <DynamicSearchCard
              search="User"
              searchBy="ID"
              onFind={handleFind}
            />
          </div>
          <div className="ml-2">
            <DynamicSearchCard
              search="User"
              searchBy="Role"
              onFind={handleFind}
            />
          </div>
          <div className="ml-2">
            <DynamicSearchCard
              search="User"
              searchBy="Location"
              onFind={handleFind}
            />
          </div>
        </div>
        <div className="max-w-4xl min-w-full overflow-y-scroll overflow-x-scroll max-h-96 bg-gray-300 rounded-lg shadow-md">
          <DynamicTable
            rows={tableData}
            columnsTitles={columnsTitles}
            title={title}
            rowLoader={getUsers}
          />
        </div>
      </div>
    </div>
  );
};

export default UserManagement;
