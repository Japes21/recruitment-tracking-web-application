import React, { useState, useEffect } from "react";
import Config from "../../../config";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

const AddUser = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("");
  const [location, setLocation] = useState("");

  const [roles, setRoles] = useState([]);
  const [locations, setLocations] = useState([]);

  let result;

  useEffect(() => {
    axios
      .get(Config.serverURL + "/admin/getAllRoles")
      .then((response) => {
        result = response.data;
        console.log(result);

        setRoles(response.data);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });

    axios
      .get(Config.serverURL + "/admin/getAllLocations")
      .then((response) => {
        setLocations(response.data);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });
  }, []);

  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();

    axios
      .post(Config.serverURL + "/admin/addUser", {
        name,
        email,
        password,
        role,
        location,
      })
      .then((response) => {
        const result1 = response.data;
        if (result1.status === "error") {
          toast.error("User Email is already registered!");
        } else {
          toast.success("Succefully registered a new user.");
          navigate("/userManagement");
        }
      })
      .catch((error) => {
        toast.error("User Email is already registered!");
        console.log("error");
        console.log(error);
      });
  };

  return (
    <div className="flex justify-center items-center mt-2">
      <form onSubmit={handleSubmit}>
        <div className="mb-2">
          <h1 className="block text-black text-lg font-semibold mb-2">
            Add User
          </h1>
        </div>
        <div className="mb-2">
          <label className="block text-left text-black font-semibold mb-2">
            Name:
          </label>
          <input
            type="text"
            value={name}
            onChange={(event) => {
              setName(event.target.value);
            }}
            required
            className="w-96 px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
          />
        </div>
        <div className="mb-2">
          <label
            htmlFor="email"
            className="block text-left text-black font-semibold mb-2"
          >
            Email:
          </label>
          <input
            type="email"
            id="email"
            value={email}
            onChange={(event) => {
              setEmail(event.target.value);
            }}
            required
            className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
          />
        </div>
        <div className="mb-2">
          <label
            htmlFor="password"
            className="block text-left text-black font-semibold mb-2"
          >
            Password:
          </label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={(event) => {
              setPassword(event.target.value);
            }}
            required
            className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
          />
        </div>
        <div className="mb-2">
          <label className="block text-left text-black font-semibold mb-2">
            Location:
          </label>
          <select
            name="location"
            value={location}
            onChange={(event) => {
              setLocation(event.target.value);
            }}
            required
            className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
          >
            <option value="">Select Location</option>
            {locations.map((location, index) => (
              <option
                key={location.location_name}
                value={location.location_name}
              >
                {location.location_name}
              </option>
            ))}
          </select>
        </div>
        <div className="mb-2">
          <label className="block text-left text-black font-semibold mb-2">
            Role:
          </label>
          <select
            name="role"
            value={role}
            onChange={(event) => {
              setRole(event.target.value);
            }}
            required
            className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
          >
            <option value="">Select Role</option>
            {roles.map((role, index) => (
              <option key={role.role_name} value={role.role_name}>
                {role.role_name}
              </option>
            ))}
          </select>
        </div>
        <button
          type="submit"
          className="w-full bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
        >
          Add User
        </button>
      </form>
    </div>
  );
};

export default AddUser;
