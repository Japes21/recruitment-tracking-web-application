import React, { useEffect, useState } from "react";
import DynamicTable from "../../../components/dynamicTable";
import axios from "axios";
import Config from "./../../../config";
import { Link } from "react-router-dom";

import DynamicSearchCard from "../../../components/dynamicSearchCard";

const DesignationManagement = () => {
  const [Designations, setDesignations] = useState([]);

  let result;

  useEffect(() => {
    getDesignations();
  }, []);

  const getDesignations = () => {
    axios
      .get(Config.serverURL + "/admin/getAllDesignations")
      .then((response) => {
        result = response.data;
        console.log(result);

        setDesignations(response.data);
      });
  };

  const tableData = Designations;
  const title = "Position Details";
  const columnsTitles = ["Position ID", "Position Name"];

  const handleFind = (searchBy, searchValue) => {
    console.log(searchValue);
    console.log({ searchBy });
    if (searchBy === "ID") {
      axios
        .get(
          Config.serverURL +
            "/admin/getDesignationByDesignationId/" +
            searchValue
        )
        .then((response) => {
          setDesignations(response.data);
        });
    }
  };

  return (
    <div>
      <div className="flex-col justify-center p-10">
        <div className="flex mb-1">
          <Link to={"/addDesignation"}>
            <button className="flex text-white font-semibold text-center p-1 rounded-full bg-gray-800 hover:bg-gray-600 active:bg-gray-500 focus:outline-none focus:ring focus:ring-white">
              Add Position
            </button>
          </Link>
          <div className="ml-2">
            <DynamicSearchCard
              search="Position"
              searchBy="ID"
              onFind={handleFind}
            />
          </div>
        </div>
        <div className="max-w-4xl min-w-full overflow-y-scroll overflow-x-scroll max-h-96 bg-gray-300 rounded-lg shadow-md">
          <DynamicTable
            rows={tableData}
            columnsTitles={columnsTitles}
            title={title}
            rowLoader={getDesignations}
          />
        </div>
      </div>
    </div>
  );
};

export default DesignationManagement;
