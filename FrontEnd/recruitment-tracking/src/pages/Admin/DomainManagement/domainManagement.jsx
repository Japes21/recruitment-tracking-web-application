import React, { useEffect, useState } from "react";
import DynamicTable from "../../../components/dynamicTable";
import axios from "axios";
import Config from "./../../../config";
import { Link } from "react-router-dom";

import DynamicSearchCard from "../../../components/dynamicSearchCard";

const DomainManagement = () => {
  const [domains, setDomains] = useState([]);

  let result;

  useEffect(() => {
    getDomains();
  }, []);

  const getDomains = () => {
    axios.get(Config.serverURL + "/admin/getAllDomains").then((response) => {
      result = response.data;
      console.log(result);

      setDomains(response.data);
    });
  };

  const tableData = domains;
  const title = "Domain Details";
  const columnsTitles = ["Domain ID", "Domain Name"];

  const handleFind = (searchBy, searchValue) => {
    console.log(searchValue);
    console.log({ searchBy });
    if (searchBy === "ID") {
      axios
        .get(Config.serverURL + "/admin/getDomainByDomainId/" + searchValue)
        .then((response) => {
          setDomains(response.data);
        });
    }
  };

  return (
    <div>
      <div className="flex-col justify-center p-10">
        <div className="flex mb-1">
          <Link to={"/addDomain"}>
            <button className="flex text-white font-semibold text-center p-1 rounded-full bg-gray-800 hover:bg-gray-600 active:bg-gray-500 focus:outline-none focus:ring focus:ring-white">
              Add Domain
            </button>
          </Link>
          <div className="ml-2">
            <DynamicSearchCard
              search="Domain"
              searchBy="ID"
              onFind={handleFind}
            />
          </div>
        </div>
        <div className="max-w-4xl min-w-full overflow-y-scroll overflow-x-scroll max-h-96 bg-gray-300 rounded-lg shadow-md">
          <DynamicTable
            rows={tableData}
            columnsTitles={columnsTitles}
            title={title}
            rowLoader={getDomains}
          />
        </div>
      </div>
    </div>
  );
};

export default DomainManagement;
