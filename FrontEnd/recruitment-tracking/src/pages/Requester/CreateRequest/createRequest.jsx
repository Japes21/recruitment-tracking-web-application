import React, { useState, useEffect } from "react";
import Config from "../../../config";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

const CreateRequest = () => {
  const [location, setLocation] = useState("");
  const [domain, setDomain] = useState("");
  const [designation, setDesignation] = useState("");
  const [headCount, setHeadCount] = useState("");
  const [deadline, setDeadline] = useState("");
  const [remark, setRemark] = useState("");
  const [userId] = useState(sessionStorage.getItem("userId"));
  const [userName] = useState(sessionStorage.getItem("userName"));
  const [jobDescription, setJobDescription] = useState();

  // const [notification, setNotification] = useState("");
  const [isRead] = useState(false);
  const [createdBy] = useState(sessionStorage.getItem("userName"));
  // const [sendTo, setSendTo] = useState("");
  const [requesterId] = useState(sessionStorage.getItem("userId"));
  const [hrId] = useState(null);

  const navigate = useNavigate();
  // console.log("dddddddd", new Date().toLocaleString());
  // console.log(new Date());

  const handleSubmit = (e) => {
    e.preventDefault();

    const formData = new FormData();

    formData.append("location", location);
    formData.append("domain", domain);
    formData.append("designation", designation);
    formData.append("headCount", headCount);
    formData.append("deadline", deadline);
    formData.append("jobDescription", jobDescription);
    formData.append("remark", remark);
    formData.append("userId", userId);
    formData.append("userName", userName);

    axios
      .post(Config.serverURL + "/requester/createRequest", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then((response) => {
        const result = response.data;

        console.log("rrrrrrrrrrrrr", result);

        const notificationText =
          // "New Request #" +
          // result.data.insertId +
          // " is created by " +
          // sessionStorage.getItem("userName") +
          // " for " +
          // headCount +
          // " " +
          // designation +
          // " at " +
          // new Date().toLocaleString() +
          // ".";
          "New Request #" +
          result.data.insertId +
          ": " +
          headCount +
          " " +
          designation +
          " requested by " +
          sessionStorage.getItem("userName");

        if (result.status === "error") {
          toast.error("Error while creting request!");
        } else {
          axios
            .post(Config.serverURL + "/addNotification", {
              notification: notificationText,
              isRead,
              createdBy,
              sendTo: "AllHr",
              requesterId,
              hrId,
              requestId: result.data.insertId,
            })
            .then((response) => {
              const result = response.data;
              if (result.status === "error") {
                console.log("notification not created");
              } else {
                console.log("notification created");
              }
            })
            .catch((error) => {
              console.log("error creating notification");
              console.log(error);
            });
          toast.success("Succefully created a new request.");
          navigate("/requestManagement");
        }
      })
      .catch((error) => {
        console.error("Error submitting form:", error);
      });
  };

  const [locations, setLocations] = useState([]);
  const [domains, setDomains] = useState([]);
  const [designations, setDesignations] = useState([]);

  let result1;
  let result2;
  let result3;

  useEffect(() => {
    axios
      .get(Config.serverURL + "/admin/getAllLocations")
      .then((response) => {
        result1 = response.data;
        console.log(result1);

        setLocations(response.data);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });

    axios
      .get(Config.serverURL + "/admin/getAllDomains")
      .then((response) => {
        result2 = response.data;
        console.log(result2);

        setDomains(response.data);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });

    axios
      .get(Config.serverURL + "/admin/getAllDesignations")
      .then((response) => {
        result3 = response.data;
        console.log(result3);

        setDesignations(response.data);
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });
  }, []);

  return (
    <div className="flex justify-center items-center mt-10">
      <form onSubmit={handleSubmit}>
        <div className="mb-4">
          <h1 className="block text-black text-lg font-semibold mb-2">
            Create Request
          </h1>
        </div>
        <div className="flex mb-4">
          <div className="flex-col">
            <label className="block text-left text-black font-semibold mb-2">
              Location:
            </label>
            <select
              name="location"
              value={location}
              onChange={(event) => {
                setLocation(event.target.value);
              }}
              required
              className="w-72 px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
            >
              <option value="">Select Location</option>
              {locations.map((loc, index) => (
                <option key={loc.location_name} value={loc.location_name}>
                  {loc.location_name}
                </option>
              ))}
            </select>
          </div>
          <div className="flex-col ml-4">
            <label className="block text-left text-black font-semibold mb-2">
              Domain:
            </label>
            <select
              name="domain"
              value={domain}
              onChange={(event) => {
                setDomain(event.target.value);
              }}
              required
              className="w-72 px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
            >
              <option value="">Select Domain</option>
              {domains.map((dom, index) => (
                <option key={dom.domain_name} value={dom.domain_name}>
                  {dom.domain_name}
                </option>
              ))}
            </select>
          </div>
        </div>
        <div className="flex mb-4">
          <div className="flex-col">
            <label className="block text-left text-black font-semibold mb-2">
              Designation:
            </label>
            <select
              name="designation"
              value={designation}
              onChange={(event) => {
                setDesignation(event.target.value);
              }}
              required
              className="w-48 px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
            >
              <option value="">Select Designation</option>
              {designations.map((des, index) => (
                <option key={des.designation_name} value={des.designation_name}>
                  {des.designation_name}
                </option>
              ))}
            </select>
          </div>
          <div className="flex-col ml-4">
            <label className="block text-left text-black font-semibold mb-2">
              Head Count:
            </label>
            <input
              type="text"
              name="headCount"
              value={headCount}
              onChange={(event) => {
                setHeadCount(event.target.value);
              }}
              required
              className="w-48 px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
            />
          </div>
          <div className="flex-col ml-4">
            <label className="block text-left text-black font-semibold mb-2">
              Deadline:
            </label>
            <input
              type="date"
              name="deadline"
              value={deadline}
              onChange={(event) => {
                setDeadline(event.target.value);
              }}
              required
              className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
            />
          </div>
        </div>
        <div className="mb-4">
          <label className="block text-left text-black font-semibold mb-2">
            Job Description:
          </label>
          <input
            type="file"
            name="jobDescription"
            onChange={(event) => {
              setJobDescription(event.target.files[0]);
            }}
            required
            className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
          />
        </div>
        <div className="mb-4">
          <label className="block text-left text-black font-semibold mb-2">
            Remark:
          </label>
          <input
            type="text"
            name="remark"
            value={remark}
            onChange={(event) => {
              setRemark(event.target.value);
            }}
            required
            className="w-full px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:border-customBlue"
          />
        </div>
        <button
          type="submit"
          className="w-full bg-customBlue text-white font-semibold py-2 px-4 rounded-lg hover:bg-sky-400 transition duration-200"
        >
          Create Request
        </button>
      </form>
    </div>
  );
};

export default CreateRequest;
