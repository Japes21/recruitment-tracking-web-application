import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import Login from "./pages/Login/login";
import HomePage from "./pages/HomePage/homePage";
import Sidebar from "./components/sidebar";
import Navbar from "./components/navbar";
import { Outlet } from "react-router-dom";
// import DynamicTable from "./components/dynamicTable";
// import DynamicSearchCard from "./components/dynamicSearchCard";

import UserManagement from "./pages/Admin/UserManagement/userManagement";
import AddUser from "./pages/Admin/UserManagement/addUser";
import LocationManagement from "./pages/Admin/LocationManagement/locationManagement";
import AddLocation from "./pages/Admin/LocationManagement/addLocation";
import DomainManagement from "./pages/Admin/DomainManagement/domainManagement";
import AddDomain from "./pages/Admin/DomainManagement/addDomain";
import DesignationManagement from "./pages/Admin/DesignationManagement/designationManagement";
import AddDesignation from "./pages/Admin/DesignationManagement/addDesignation";
import RequestStatusManagement from "./pages/Admin/RequestStatusManagement/requestStatusManagement";
import AddRequestStatus from "./pages/Admin/RequestStatusManagement/addRequestStatus";
import CandidateStatusManagement from "./pages/Admin/CandidateStatusManagement/candidateStatusManagement";
import AddCandidateStatus from "./pages/Admin/CandidateStatusManagement/addCandidateStatus";
import CreateRequest from "./pages/Requester/CreateRequest/createRequest";
import RequestManagement from "./pages/Admin/RequestManagement/requestManagement";
import CandidateManagement from "./pages/Admin/Candidate Management/candidateManagement";

const Layout = () => {
  return (
    <div className="App flex">
      <Sidebar role={sessionStorage.getItem("userRole")} />
      <div className="NavbarContainer flex-1">
        <Navbar />
        <div className="content">
          <Outlet />
        </div>
      </div>
    </div>
  );
};

function App() {
  return (
    <BrowserRouter>
      <ToastContainer position="top-center" autoClose={1500} />
      <Routes>
        <Route path="/" element={<Login />} />

        <Route element={<Layout />}>
          <Route path="/homePage" element={<HomePage />} />
          {/* <Route path="/dynamicTable" element={<DynamicTable />} />
          <Route path="/dynamicSearchCard" element={<DynamicSearchCard />} /> */}

          <Route path="/userManagement" element={<UserManagement />} />
          <Route path="/addUser" element={<AddUser />} />
          <Route path="/locationManagement" element={<LocationManagement />} />
          <Route path="/addLocation" element={<AddLocation />} />
          <Route path="/domainManagement" element={<DomainManagement />} />
          <Route path="/addDomain" element={<AddDomain />} />
          <Route
            path="/designationManagement"
            element={<DesignationManagement />}
          />
          <Route path="/addDesignation" element={<AddDesignation />} />
          <Route
            path="/requestStatusManagement"
            element={<RequestStatusManagement />}
          />
          <Route path="/addRequestStatus" element={<AddRequestStatus />} />
          <Route
            path="/candidateStatusManagement"
            element={<CandidateStatusManagement />}
          />
          <Route path="/addCandidateStatus" element={<AddCandidateStatus />} />
          <Route path="/createRequest" element={<CreateRequest />} />
          <Route path="/requestManagement" element={<RequestManagement />} />
          <Route
            path="/candidateManagement"
            element={<CandidateManagement />}
          />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
