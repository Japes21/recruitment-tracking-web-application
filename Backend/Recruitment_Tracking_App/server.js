const express = require("express");
var cors = require("cors");
const path = require("path");

const app = express();

app.use(express.json());
app.use(cors());

const adminRouter = require("./routes/admin");
const requesterRouter = require("./routes/requester");
const hrRouter = require("./routes/hr");
const loginRouter = require("./routes/login");
const notificationRouter = require("./routes/notification");

const staticPath = path.join(__dirname, "/", "static");
app.use(express.static(staticPath));

app.use("/admin", adminRouter);
app.use("/requester", requesterRouter);
app.use("/hr", hrRouter);
app.use(loginRouter);
app.use(notificationRouter);

app.get("/*", function (req, res) {
  res.sendFile(path.join(staticPath, "index.html"));
});

app.listen(4000, () => {
  console.log("server started on port 4000");
});
