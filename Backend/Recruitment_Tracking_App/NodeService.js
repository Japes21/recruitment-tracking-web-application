const Service = require("node-windows").Service;
const svc = new Service({
  name: "nodeBasicServer",
  description: "Recruitment Tracking",
  script:
    "C:\\recruitment-tracking-web-application\\Backend\\Recruitment_Tracking_App\\server.js",
});

svc.on("install", function () {
  svc.start();
});

svc.install();
