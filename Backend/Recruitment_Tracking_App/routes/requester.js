const express = require("express");
const db = require("../db");
const path = require("path");
const multer = require("multer");
const fs = require("fs");
const router = express.Router();
const utils = require("../utils");

const upload = multer({ dest: "uploads/" });

router.post("/createRequest", upload.single("jobDescription"), (req, res) => {
  const {
    location,
    domain,
    designation,
    headCount,
    deadline,
    remark,
    userId,
    userName,
  } = req.body;
  const jobDescription = req.file;

  console.log(jobDescription);
  console.log("req", req.file.originalname.split(".").pop());

  const fName =
    req.file.filename + "." + req.file.originalname.split(".").pop();

  const { path } = req.file;
  console.log(path);

  const newPath = path + "." + req.file.originalname.split(".").pop();
  console.log(newPath);
  fs.renameSync(path, newPath);

  const sql = `INSERT INTO request_details (location, domain, designation, head_count, deadline, remark, job_description, request_status, user_id, user_name) VALUES (?, ?, ?, ?, ?, ?, ?, 'pending', ?, ?)`;

  db.pool.query(
    sql,
    [
      location,
      domain,
      designation,
      headCount,
      deadline,
      remark,
      fName,
      userId,
      userName,
    ],
    (error, result) => {
      res.send(utils.createResult(error, result));
    }
  );
});

router.put(
  "/editRequest/:requestId",
  upload.single("jobDescription"),
  (req, res) => {
    const { location, domain, designation, headCount, deadline, remark } =
      req.body;
    const { requestId } = req.params;
    const sql = `SELECT job_description FROM request_details where request_id=?`;

    db.pool.query(sql, [requestId], (error, result) => {
      if (error) {
        res.send(utils.createResult(error, null));
      } else {
        const filenav = result[0].job_description;

        const filePath = path.join("./uploads", filenav);

        fs.readFile(filePath, (err, data) => {
          if (err) {
            console.error(`Error reading file: ${err}`);
            return res.send(utils.createResult(err, null));
          }

          const fileDetails = {
            originalname: filenav,
            filename: filenav.split(".")[0],
            content: data,
            size: data.length,
          };

          console.log("filename", fileDetails.filename);

          const jobDescription = req.file || fileDetails;

          const fName =
            jobDescription.filename +
            "." +
            jobDescription.originalname.split(".").pop();

          console.log("aaaaaaaaaaaa", fName);

          const newPath = path.join("./uploads", fName);

          fs.renameSync(filePath, newPath);

          const sql1 = `UPDATE 
                        request_details 
                       SET
                        location = IFNULL(NULLIF('${location}',''), location),
                        domain = IFNULL(NULLIF('${domain}',''), domain),
                        designation = IFNULL(NULLIF('${designation}',''), designation),
                        head_count = IFNULL(NULLIF('${headCount}',''), head_count),
                        deadline = IFNULL(NULLIF('${deadline}',''), deadline),
                        job_description = IFNULL(NULLIF('${fName}',''), job_description),
                        remark = IFNULL(NULLIF('${remark}',''), remark)
                       WHERE
                        request_id = ?`;

          db.pool.query(sql1, [requestId], (error, result) => {
            res.send(utils.createResult(error, result));
          });
        });
      }
    });
  }
);

router.get("/getAllRequestsByUserId/:userId", (request, response) => {
  const { userId } = request.params;
  const statement = `
    SELECT 
      request_id,user_id,user_name,request_status,location,domain,designation,head_count,deadline,remark,job_description 
    FROM 
      request_details
    WHERE
      user_Id = ?
  `;
  db.pool.query(statement, [userId], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.put("/updateRequestStatus/:requestId", (request, response) => {
  const { requestId } = request.params;
  const { requestStatus } = request.body;
  const statement = `
    UPDATE 
      request_details
    SET  
      request_status = ?
    WHERE
      request_id = ?
  `;
  db.pool.query(statement, [requestStatus, requestId], (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.get(
  "/getRequestByRequestId&UserId/:requestId/:userId",
  (request, response) => {
    const { requestId } = request.params;
    const { userId } = request.params;
    const statement = `
      SELECT 
        request_id,user_id,user_name,request_status,location,domain,designation,head_count,deadline,remark,job_description 
      FROM 
        request_details
      WHERE
        request_Id = ? AND user_Id = ?
  `;
    db.pool.query(statement, [requestId, userId], (error, users) => {
      if (error) {
        response.send(error);
      } else {
        response.send(users);
      }
    });
  }
);

router.get(
  "/getAllRequestsByLocation&UserId/:location/:userId",
  (request, response) => {
    const { location } = request.params;
    const { userId } = request.params;
    const statement = `
    SELECT 
      request_id,user_id,user_name,request_status,location,domain,designation,head_count,deadline,remark,job_description 
    FROM 
      request_details
    WHERE
      location = ? AND user_Id = ?
  `;
    db.pool.query(statement, [location, userId], (error, users) => {
      if (error) {
        response.send(error);
      } else {
        response.send(users);
      }
    });
  }
);

router.get(
  "/getAllRequestsByDomain&UserId/:domain/:userId",
  (request, response) => {
    const { domain } = request.params;
    const { userId } = request.params;
    const statement = `
    SELECT 
      request_id,user_id,user_name,request_status,location,domain,designation,head_count,deadline,remark,job_description 
    FROM 
      request_details
    WHERE
      domain = ? AND user_Id = ?
  `;
    db.pool.query(statement, [domain, userId], (error, users) => {
      if (error) {
        response.send(error);
      } else {
        response.send(users);
      }
    });
  }
);

router.get(
  "/getAllRequestsByDesignation&UserId/:designation/:userId",
  (request, response) => {
    const { designation } = request.params;
    const { userId } = request.params;
    const statement = `
      SELECT 
        request_id,user_id,user_name,request_status,location,domain,designation,head_count,deadline,remark,job_description 
      FROM 
        request_details
      WHERE
        designation = ? AND user_Id = ?
  `;
    db.pool.query(statement, [designation, userId], (error, users) => {
      if (error) {
        response.send(error);
      } else {
        response.send(users);
      }
    });
  }
);

router.get(
  "/getAllRequestsByStatus&UserId/:rqStatus/:userId",
  (request, response) => {
    const { rqStatus } = request.params;
    const { userId } = request.params;
    const statement = `
    SELECT 
      request_id,user_id,user_name,request_status,location,domain,designation,head_count,deadline,remark,job_description 
    FROM 
      request_details
    WHERE
      request_status = ? AND user_Id = ?
  `;
    db.pool.query(statement, [rqStatus, userId], (error, users) => {
      if (error) {
        response.send(error);
      } else {
        response.send(users);
      }
    });
  }
);

router.get("/getAllCandidatesByUserId/:userId", (request, response) => {
  const { userId } = request.params;
  const statement = `
    SELECT 
      candidate_id,candidate_name,request_id,requester_name,user_name,resume,candidate_status,requester_id,user_id
    FROM 
      candidate_details
    WHERE
      user_Id = ?
  `;
  db.pool.query(statement, [userId], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.get(
  "/getAllCandidatesByRequesterId/:requesterId",
  (request, response) => {
    const { requesterId } = request.params;
    const statement = `
    SELECT 
      candidate_id,candidate_name,request_id,requester_name,user_name,resume,candidate_status,requester_id,user_id
    FROM 
      candidate_details
    WHERE
      requester_id = ?
  `;
    db.pool.query(statement, [requesterId], (error, users) => {
      if (error) {
        response.send(error);
      } else {
        response.send(users);
      }
    });
  }
);

router.put("/updateCandidateStatus/:cndId", (request, response) => {
  const { cndId } = request.params;
  const { candidateStatus } = request.body;
  const statement = `
    UPDATE 
      candidate_details
    SET  
      candidate_status = ?
    WHERE
      candidate_id = ?
  `;
  db.pool.query(statement, [candidateStatus, cndId], (error, result) => {
    if (error) {
      response.send(error);
    } else {
      response.send(result);
    }
  });
});

router.get(
  "/getCandidateByCandidateId&UserId/:cndId/:userId",
  (request, response) => {
    const { cndId } = request.params;
    const { userId } = request.params;
    const statement = `
      SELECT 
        candidate_id,candidate_name,request_id,requester_name,user_name,resume,candidate_status,requester_id,user_id
      FROM 
        candidate_details
      WHERE
        candidate_Id = ? AND user_Id = ?
  `;
    db.pool.query(statement, [cndId, userId], (error, users) => {
      if (error) {
        response.send(error);
      } else {
        response.send(users);
      }
    });
  }
);

router.get(
  "/getCandidateByCandidateId&RequesterId/:cndId/:requesterId",
  (request, response) => {
    const { cndId } = request.params;
    const { requesterId } = request.params;
    const statement = `
      SELECT 
        candidate_id,candidate_name,request_id,requester_name,user_name,resume,candidate_status,requester_id,user_id
      FROM 
        candidate_details
      WHERE
        candidate_Id = ? AND requester_Id = ?
  `;
    db.pool.query(statement, [cndId, requesterId], (error, users) => {
      if (error) {
        response.send(error);
      } else {
        response.send(users);
      }
    });
  }
);

router.get(
  "/getCandidateByRequestId&UserId/:rqId/:userId",
  (request, response) => {
    const { rqId } = request.params;
    const { userId } = request.params;
    const statement = `
      SELECT 
        candidate_id,candidate_name,request_id,requester_name,user_name,resume,candidate_status,requester_id,user_id
      FROM 
        candidate_details
      WHERE
        request_Id = ? AND user_Id = ?
  `;
    db.pool.query(statement, [rqId, userId], (error, users) => {
      if (error) {
        response.send(error);
      } else {
        response.send(users);
      }
    });
  }
);

router.get(
  "/getAllCandidatesByRequestId&RequesterId/:requestId/:requesterId",
  (request, response) => {
    const { requestId } = request.params;
    const { requesterId } = request.params;
    const statement = `
      SELECT 
        candidate_id,candidate_name,request_id,requester_name,user_name,resume,candidate_status,requester_id,user_id
      FROM 
        candidate_details
      WHERE
        request_Id = ? AND requester_Id = ?
  `;
    db.pool.query(statement, [requestId, requesterId], (error, users) => {
      if (error) {
        response.send(error);
      } else {
        response.send(users);
      }
    });
  }
);

router.get(
  "/getAllCandidatesByCandidateStatus&UserId/:userId",
  (request, response) => {
    const { cndStatus } = request.body;
    const { userId } = request.params;
    const statement = `
      SELECT 
        candidate_id,candidate_name,request_id,requester_name,user_name,resume,candidate_status,requester_id,user_id
      FROM 
        candidate_details
      WHERE
        candidate_status = ? AND user_Id = ?
  `;
    db.pool.query(statement, [cndStatus, userId], (error, users) => {
      if (error) {
        response.send(error);
      } else {
        response.send(users);
      }
    });
  }
);

router.get(
  "/getAllCandidatesByCandidateStatus&RequesterId/:candidateStatus/:requesterId",
  (request, response) => {
    const { candidateStatus } = request.params;
    const { requesterId } = request.params;
    const statement = `
      SELECT 
        candidate_id,candidate_name,request_id,requester_name,user_name,resume,candidate_status,requester_id,user_id
      FROM 
        candidate_details
      WHERE
        candidate_status = ? AND requester_Id = ?
  `;
    db.pool.query(statement, [candidateStatus, requesterId], (error, users) => {
      if (error) {
        response.send(error);
      } else {
        response.send(users);
      }
    });
  }
);

router.get("/getCountOfAllRequestsByUserId/:userId", (request, response) => {
  const { userId } = request.params;
  const statement = `
    SELECT 
      COUNT(*) as createdCount FROM request_details
    WHERE 
      user_id = ?
  `;
  db.pool.query(statement, [userId], (error, requests) => {
    if (error) {
      response.send(error);
    } else {
      response.send(requests);
    }
  });
});

router.get(
  "/getCountOfPendingRequestsByUserId/:userId",
  (request, response) => {
    const { userId } = request.params;
    const statement = `
      SELECT 
        COUNT(*) as pendingCountByUser FROM request_details
      WHERE
        request_status = "pending" AND user_id = ?
  `;
    db.pool.query(statement, [userId], (error, requests) => {
      if (error) {
        response.send(error);
      } else {
        response.send(requests);
      }
    });
  }
);

router.get(
  "/getCountOfFulfilledRequestsByUserId/:userId",
  (request, response) => {
    const { userId } = request.params;
    const statement = `
      SELECT 
        COUNT(*) as fulfilledCountByUser FROM request_details
      WHERE
        request_status = "fulfilled" AND user_id = ?
  `;
    db.pool.query(statement, [userId], (error, requests) => {
      if (error) {
        response.send(error);
      } else {
        response.send(requests);
      }
    });
  }
);

router.get("/downloadResume/:file", (req, res) => {
  const { file } = req.params;
  const filePath = path.join("./resume_uploads", file);
  res.download(filePath, (err) => {
    if (err) {
      console.error("Error downloading file:", err);
      res.status(404).json({ error: "File not found." });
    }
  });
});

module.exports = router;
