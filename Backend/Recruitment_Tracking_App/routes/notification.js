const express = require("express");
const db = require("../db");
const utils = require("../utils");

const router = express.Router();

router.post("/addNotification", (request, response) => {
  const {
    notification,
    isRead,
    createdBy,
    sendTo,
    requesterId,
    hrId,
    requestId,
    createdAt,
  } = request.body;

  const statement = `
      INSERT INTO notification_details
        (notification, is_read, created_by, send_to, requester_id, hr_id,request_id,created_at)
      VALUES
        (?,?,?,?,?,?,?,now())  
    `;
  db.pool.query(
    statement,
    [
      notification,
      isRead,
      createdBy,
      sendTo,
      requesterId,
      hrId,
      requestId,
      createdAt,
    ],
    (error, result) => {
      response.send(utils.createResult(error, result));
    }
  );
});

router.get("/getCountOfUnreadNotifications", (request, response) => {
  const statement = `
  select count(notification_id) as unread_notifications from notification_details where is_read=false  `;
  db.pool.query(statement, (error, count) => {
    if (error) {
      response.send(error);
    } else {
      response.send(count);
    }
  });
});

router.get(
  "/getCountOfUnreadNotificationsForRequester/:requesterId",
  (request, response) => {
    const { requesterId } = request.params;
    const statement = `
  select count(notification_id) as unread_notifications from notification_details where is_read=false and requester_id=?`;
    db.pool.query(statement, [requesterId], (error, count) => {
      if (error) {
        response.send(error);
      } else {
        response.send(count);
      }
    });
  }
);

router.get("/getAllNotifications", (request, response) => {
  const statement = `
  select notification_id,notification,is_read,created_at FROM recruitment_tracking.notification_details order by notification_id desc  `;
  db.pool.query(statement, (error, notifications) => {
    if (error) {
      response.send(error);
    } else {
      response.send(notifications);
    }
  });
});

router.get(
  "/getAllNotificationsForRequester/:requesterId",
  (request, response) => {
    const { requesterId } = request.params;
    const statement = `
  select notification_id,notification,is_read,created_at FROM recruitment_tracking.notification_details where requester_id=? order by notification_id desc`;
    db.pool.query(statement, [requesterId], (error, notifications) => {
      if (error) {
        response.send(error);
      } else {
        response.send(notifications);
      }
    });
  }
);

module.exports = router;
