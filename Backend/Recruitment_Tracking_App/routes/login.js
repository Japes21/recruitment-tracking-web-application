const express = require("express");
const db = require("../db");
const cryptoJs = require("crypto-js");

const router = express.Router();

router.post("/login", (request, response) => {
  const { email, password } = request.body;
  const encryptedPassword = String(cryptoJs.MD5(password));
  const statement = `
    SELECT 
      user_id, user_email, user_name, user_role
    FROM 
      user_details
    WHERE 
      user_email = ? AND user_password = ?
    `;
  db.pool.query(statement, [email, encryptedPassword], (error, users) => {
    const result = {};
    if (error) {
      result["status"] = "error";
      result["error"] = error;
    } else if (users.length === 0) {
      result["status"] = "error";
      result["error"] = "user not found";
    } else {
      // get the first user from the array
      const user = users[0];
      result["status"] = "success";
      result["data"] = {
        id: user["user_id"],
        name: user["user_name"],
        email: user["user_email"],
        role: user["user_role"],
      };
    }
    response.send(result);
  });
});

router.get("/getUserByUserEmail", (request, response) => {
  const { userEmail } = request.body;
  const statement = `
        SELECT 
          user_id, user_name, user_email, user_role
        FROM user_details
        WHERE
        user_email = ?
  `;
  db.pool.query(statement, [userEmail], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

module.exports = router;
