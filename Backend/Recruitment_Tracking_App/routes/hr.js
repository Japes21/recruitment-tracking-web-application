const express = require("express");
const db = require("../db");
const utils = require("../utils");
const multer = require("multer");
const fs = require("fs");
const path = require("path");

const router = express.Router();

const upload = multer({ dest: "resume_uploads/" });

router.post(
  "/provideCandidate",
  upload.single("candidateResume"),
  (req, res) => {
    const {
      candidateName,
      requestId,
      userId,
      userName,
      requesterId,
      requesterName,
    } = req.body;
    const candidateResume = req.file;

    console.log("Resume", candidateResume);

    const fName =
      req.file.filename + "." + req.file.originalname.split(".").pop();

    const { path } = req.file;
    const newPath = path + "." + req.file.originalname.split(".").pop();
    fs.renameSync(path, newPath);

    const sql =
      "INSERT INTO candidate_details (candidate_name, request_id, user_id, candidate_status, resume, user_name, requester_id, requester_name) VALUES (?, ?, ?, 'Submitted', ?, ?, ?, ?)";

    db.pool.query(
      sql,
      [
        candidateName,
        requestId,
        userId,
        fName,
        userName,
        requesterId,
        requesterName,
      ],
      (error, result) => {
        res.send(utils.createResult(error, result));
      }
    );
  }
);

router.put(
  "/editCandidate/:candidateId",
  upload.single("candidateResume"),
  (req, res) => {
    const { candidateName } = req.body;
    const { candidateId } = req.params;
    const sql = `SELECT resume FROM candidate_details where candidate_id=?`;

    db.pool.query(sql, [candidateId], (error, result) => {
      if (error) {
        res.send(utils.createResult(error, null));
      } else {
        const filenav = result[0].resume;

        const filePath = path.join("./resume_uploads", filenav);

        fs.readFile(filePath, (err, data) => {
          if (err) {
            console.error(`Error reading file: ${err}`);
            return res.send(utils.createResult(err, null));
          }

          const fileDetails = {
            originalname: filenav,
            filename: filenav.split(".")[0],
            content: data,
            size: data.length,
          };

          console.log("filename", fileDetails.filename);

          const candidateResume = req.file || fileDetails;

          const fName =
            candidateResume.filename +
            "." +
            candidateResume.originalname.split(".").pop();

          console.log("check", fName);

          const newPath = path.join("./resume_uploads", fName);

          fs.renameSync(filePath, newPath);

          const sql1 = `UPDATE 
                        candidate_details 
                       SET
                        candidate_name = IFNULL(NULLIF('${candidateName}',''), candidate_name),  
                        resume = IFNULL(NULLIF('${fName}',''), resume)   
                       WHERE
                        candidate_id = ?`;

          db.pool.query(sql1, [candidateId], (error, result) => {
            res.send(utils.createResult(error, result));
          });
        });
      }
    });
  }
);

router.get("/downloadJD/:file", (req, res) => {
  const { file } = req.params;
  const filePath = path.join("./uploads", file);

  res.download(filePath, (err) => {
    if (err) {
      console.error("Error downloading file:", err);
      res.status(404).json({ error: "File not found." });
    }
  });
});

module.exports = router;
