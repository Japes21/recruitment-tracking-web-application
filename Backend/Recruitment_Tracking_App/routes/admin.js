const express = require("express");
const db = require("../db");
const cryptoJs = require("crypto-js");
const utils = require("../utils");

const router = express.Router();

router.post("/addUser", (request, response) => {
  const { name, email, password, role, location } = request.body;
  //encrypt password
  const encryptedPassword = String(cryptoJs.MD5(password));

  const statement = `
    INSERT INTO user_details
      (user_name, user_email, user_password, user_role, user_location)
    VALUES
      (?,?,?,?,?)  
  `;
  db.pool.query(
    statement,
    [name, email, encryptedPassword, role, location],
    (error, result) => {
      response.send(utils.createResult(error, result));
    }
  );
});

// router.put("/editUser/:userId", (request, response) => {
//   //const { name, email, password, role, location } = request.body;
//   const { userId } = request.params;
//   const sql = `SELECT user_email, user_password FROM user_details where user_id=?`;
//   db.pool.query(sql, [userId], (error, result) => {
//     if (error) {
//       response.send(error);
//     } else {
//       const UserPassword = result[0].user_password;
//       const decryptedPassword = cryptoJs
//         .MD5(UserPassword)
//         .toString(cryptoJs.enc.Utf8);
//       console.log("Password is ", decryptedPassword);
//     }
//   });
// const statement = `
//   UPDATE
//     user_details
//   SET
//     user_name = IFNULL(NULLIF('${name}', ''), user_name),
//     user_email =IFNULL(NULLIF('${email}', ''), user_email),
//     user_password =IFNULL(NULLIF('${password}', ''), user_password),
//     user_role =IFNULL(NULLIF('${role}', ''), user_role),
//     user_location =IFNULL(NULLIF('${location}', ''), user_location)
//   WHERE
//     user_id = ?
// `;
// db.pool.query(statement, [userId], (error, users) => {
//   if (error) {
//     response.send(error);
//   } else {
//     response.send(users);
//   }
// });
//});

router.put("/editUserById/:userId", (request, response) => {
  const { name, email, role, location } = request.body;
  const { userId } = request.params;
  const statement = `
    UPDATE
      user_details
    SET
      user_name = IFNULL(NULLIF('${name}', ''), user_name),
      user_email =IFNULL(NULLIF('${email}', ''), user_email),
      user_role =IFNULL(NULLIF('${role}', ''), user_role),
      user_location =IFNULL(NULLIF('${location}', ''), user_location)
    WHERE
      user_id = ?
  `;
  db.pool.query(statement, [userId], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

// router.put("/editUser/:userId", (request, response) => {
//   const { name, email, password, role, location } = request.body;
//   const encryptedPassword = String(cryptoJs.MD5(password));
//   const { userId } = request.params;
//   const statement = `
//     UPDATE
//       user_details
//     SET
//       user_name = ?, user_email = ?, user_password = ?, user_role = ?, user_location = ?
//     WHERE
//       user_id = ?
//   `;
//   db.pool.query(
//     statement,
//     [name, email, encryptedPassword, role, location, userId],
//     (error, users) => {
//       if (error) {
//         response.send(error);
//       } else {
//         response.send(users);
//       }
//     }
//   );
// });

router.put("/editRoleById/:roleId", (request, response) => {
  const { role } = request.body;
  const { roleId } = request.params;
  const statement = `
    UPDATE 
      role_details
    SET 
      role_name = IFNULL(NULLIF('${role}', ''), role_name)   
    WHERE 
      role_id = ? 
  `;
  db.pool.query(statement, [roleId], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.put("/editDesignationById/:designationId", (request, response) => {
  const { designation } = request.body;
  const { designationId } = request.params;
  const statement = `
    UPDATE 
      designation_details
    SET 
      designation_name = IFNULL(NULLIF('${designation}', ''), designation_name)
    WHERE 
      designation_id = ? 
  `;
  db.pool.query(statement, [designationId], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.put("/editDomainById/:domainId", (request, response) => {
  const { domain } = request.body;
  const { domainId } = request.params;
  const statement = `
    UPDATE 
      domain_details
    SET 
      domain_name = IFNULL(NULLIF('${domain}', ''), domain_name)
    WHERE 
      domain_id = ? 
  `;
  db.pool.query(statement, [domainId], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.put("/editLocationById/:locationId", (request, response) => {
  const { location } = request.body;
  const { locationId } = request.params;
  const statement = `
    UPDATE 
      location_details
    SET 
      location_name = IFNULL(NULLIF('${location}', ''), location_name) 
    WHERE 
      location_id = ? 
  `;
  db.pool.query(statement, [locationId], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.put("/editRequestStatusById/:requestStatusId", (request, response) => {
  const { requestStatusId } = request.params;
  const { requestStatusName } = request.body;
  const statement = `
    UPDATE 
      request_status_details
    SET 
      rq_st_name = IFNULL(NULLIF('${requestStatusName}', ''), rq_st_name)   
    WHERE 
      rq_st_id = ? 
  `;
  db.pool.query(statement, [requestStatusId], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.put(
  "/editCandidateStatusById/:candidateStatusId",
  (request, response) => {
    const { candidateStatusId } = request.params;
    const { candidateStatusName } = request.body;
    const statement = `
    UPDATE 
      candidate_status_details
    SET 
      cnd_st_name = IFNULL(NULLIF('${candidateStatusName}', ''), cnd_st_name)   
    WHERE 
      cnd_st_id = ? 
  `;
    db.pool.query(statement, [candidateStatusId], (error, users) => {
      if (error) {
        response.send(error);
      } else {
        response.send(users);
      }
    });
  }
);

router.get("/getAllUsers", (request, response) => {
  const statement = `
    SELECT user_id, user_name, user_email, user_role, user_location FROM user_details
  `;
  db.pool.query(statement, (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.post("/addRole", (request, response) => {
  const { roleName } = request.body;
  const statement = `
    INSERT INTO role_details
      (role_name)
    VALUES
      (?)  
  `;
  db.pool.query(statement, [roleName], (error, result) => {
    if (error) {
      response.send(error);
    } else {
      response.send(result);
    }
  });
});

router.get("/getAllRoles", (request, response) => {
  const statement = `
    SELECT role_name FROM role_details
  `;
  db.pool.query(statement, (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.get("/getUserByUserId/:userId", (request, response) => {
  const { userId } = request.params;
  const statement = `
    SELECT 
      user_id, user_name, user_email, user_role, user_location
    FROM 
      user_details
    WHERE
      user_Id = ?
  `;
  db.pool.query(statement, [userId], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.get("/getUserByUserEmail", (request, response) => {
  const { userEmail } = request.body;
  const statement = `
    SELECT 
      user_id, user_name, user_email, user_role, user_location
    FROM 
      user_details
    WHERE
      user_email = ?
  `;
  db.pool.query(statement, [userEmail], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.get("/getUserByUserRole/:userRole", (request, response) => {
  const { userRole } = request.params;
  const statement = `
    SELECT 
      user_id, user_name, user_email, user_role, user_location
    FROM 
      user_details
    WHERE
      user_role = ?
  `;
  db.pool.query(statement, [userRole], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.get("/getUserByUserLocation/:userLocation", (request, response) => {
  const { userLocation } = request.params;
  const statement = `
    SELECT 
      user_id, user_name, user_email, user_role, user_location
    FROM 
      user_details
    WHERE
      user_location = ?
  `;
  db.pool.query(statement, [userLocation], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.get("/getCountOfAllRequests", (request, response) => {
  const statement = `
    SELECT COUNT(*) as totalCount FROM request_details
  `;
  db.pool.query(statement, (error, requests) => {
    if (error) {
      response.send(error);
    } else {
      response.send(requests);
    }
  });
});

router.get("/getCountOfPendingRequests", (request, response) => {
  const statement = `
    SELECT 
      COUNT(*) as pendingCount FROM request_details
    WHERE
      request_status = "pending"
  `;
  db.pool.query(statement, (error, requests) => {
    if (error) {
      response.send(error);
    } else {
      response.send(requests);
    }
  });
});

router.get("/getCountOfFulfilledRequests", (request, response) => {
  const statement = `
    SELECT 
      COUNT(*) as fulfilledCount FROM request_details
    WHERE
      request_status = "fulfilled"
  `;
  db.pool.query(statement, (error, requests) => {
    if (error) {
      response.send(error);
    } else {
      response.send(requests);
    }
  });
});

router.post("/addLocation", (request, response) => {
  const { locationName } = request.body;
  const statement = `
    INSERT INTO location_details
      (location_name)
    VALUES
      (?)  
  `;
  db.pool.query(statement, [locationName], (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.get("/getAllLocations", (request, response) => {
  const statement = `
    SELECT * FROM location_details
  `;
  db.pool.query(statement, (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.get("/getLocationByLocationId/:locationId", (request, response) => {
  const { locationId } = request.params;
  const statement = `
    SELECT 
      location_id, location_name FROM location_details
    WHERE
      location_id = ?
  `;
  db.pool.query(statement, [locationId], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.post("/addDomain", (request, response) => {
  const { domainName } = request.body;
  const statement = `
    INSERT INTO domain_details
      (domain_name)
    VALUES
      (?)  
  `;
  db.pool.query(statement, [domainName], (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.get("/getAllDomains", (request, response) => {
  const statement = `
    SELECT * FROM domain_details
  `;
  db.pool.query(statement, (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.get("/getDomainByDomainId/:domainId", (request, response) => {
  const { domainId } = request.params;
  const statement = `
    SELECT 
      location_id, location_name
    FROM 
      location_details
    WHERE
      location_id = ?
  `;
  db.pool.query(statement, [domainId], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.post("/addDesignation", (request, response) => {
  const { designationName } = request.body;
  const statement = `
    INSERT INTO designation_details
      (designation_name)
    VALUES
      (?)  
  `;
  db.pool.query(statement, [designationName], (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.get("/getAllDesignations", (request, response) => {
  const statement = `
    SELECT * FROM designation_details
  `;
  db.pool.query(statement, (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.get(
  "/getDesignationByDesignationId/:designationId",
  (request, response) => {
    const { designationId } = request.params;
    const statement = `
      SELECT 
        designation_id, designation_name FROM designation_details
      WHERE
        designation_id = ?
  `;
    db.pool.query(statement, [designationId], (error, users) => {
      if (error) {
        response.send(error);
      } else {
        response.send(users);
      }
    });
  }
);

router.post("/addRequestStatus", (request, response) => {
  const { requestStatusName } = request.body;
  const statement = `
    INSERT INTO request_status_details
      (rq_st_name)
    VALUES
      (?)  
  `;
  db.pool.query(statement, [requestStatusName], (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.get("/getAllRequestStatus", (request, response) => {
  const statement = `
    SELECT * FROM request_status_details
  `;
  db.pool.query(statement, (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.post("/addCandidateStatus", (request, response) => {
  const { candidateStatusName } = request.body;
  const statement = `
    INSERT INTO candidate_status_details
      (cnd_st_name)
    VALUES
      (?)  
  `;
  db.pool.query(statement, [candidateStatusName], (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.get("/getAllCandidateStatus", (request, response) => {
  const statement = `
    SELECT * FROM candidate_status_details
  `;
  db.pool.query(statement, (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.get("/getAllRequests", (request, response) => {
  const statement = `
    SELECT 
      request_id,user_id,user_name,request_status,location,domain,designation,head_count,deadline,remark,job_description 
    FROM 
      request_details
  `;
  db.pool.query(statement, (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.get("/getRequestByRequestId/:rqId", (request, response) => {
  const { rqId } = request.params;

  const statement = `
    SELECT 
      request_id,user_id,user_name,request_status,location,domain,designation,head_count,deadline,remark,job_description 
    FROM 
      request_details
    WHERE
      request_id = ? 
  `;
  db.pool.query(statement, [rqId], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.get("/getAllRequestsByLocation/:location", (request, response) => {
  const { location } = request.params;
  const statement = `
  SELECT 
    request_id,user_id,user_name,request_status,location,domain,designation,head_count,deadline,remark,job_description 
  FROM
    request_details
  WHERE 
    location = ? 
`;
  db.pool.query(statement, [location], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.get("/getAllRequestsByDomain/:domain", (request, response) => {
  const { domain } = request.params;
  const statement = `
  SELECT 
    request_id,user_id,user_name,request_status,location,domain,designation,head_count,deadline,remark,job_description 
  FROM
    request_details
  WHERE 
    domain = ? 
`;
  db.pool.query(statement, [domain], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.get("/getAllRequestsByDesignation/:designation", (request, response) => {
  const { designation } = request.params;
  const statement = `
  SELECT 
    request_id,user_id,user_name,request_status,location,domain,designation,head_count,deadline,remark,job_description 
  FROM
    request_details
  WHERE 
    designation = ? 
`;
  db.pool.query(statement, [designation], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.get("/getAllRequestsByStatus/:rqStatus", (request, response) => {
  const { rqStatus } = request.params;
  const statement = `
  SELECT 
    request_id,user_id,user_name,request_status,location,domain,designation,head_count,deadline,remark,job_description 
  FROM
    request_details
  WHERE 
    request_status = ? 
`;
  db.pool.query(statement, [rqStatus], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.get("/getAllCandidates", (request, response) => {
  const statement = `
    SELECT 
      candidate_id,candidate_name,request_id,requester_name,user_name,resume,candidate_status,requester_id,user_id
    FROM 
      candidate_details
  `;
  db.pool.query(statement, (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.get("/getCandidateByCandidateId/:cndId", (request, response) => {
  const { cndId } = request.params;
  const statement = `
    SELECT 
      candidate_id,candidate_name,request_id,requester_name,user_name,resume,candidate_status,requester_id,user_id
    FROM
      candidate_details
    WHERE
      candidate_Id = ?
  `;
  db.pool.query(statement, [cndId], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.get("/getCandidatesByRequestId/:rqId", (request, response) => {
  const { rqId } = request.params;
  const statement = `
    SELECT 
      candidate_id,candidate_name,request_id,requester_name,user_name,resume,candidate_status,requester_id,user_id
    FROM 
      candidate_details
    WHERE
      request_Id = ?
  `;
  db.pool.query(statement, [rqId], (error, users) => {
    if (error) {
      response.send(error);
    } else {
      response.send(users);
    }
  });
});

router.get(
  "/getCandidatesByCandidateStatus/:cndStatus",
  (request, response) => {
    const { cndStatus } = request.params;
    const statement = `
      SELECT 
        candidate_id,candidate_name,request_id,requester_name,user_name,resume,candidate_status,requester_id,user_id
      FROM 
        candidate_details
      WHERE 
        candidate_status = ?
`;
    db.pool.query(statement, [cndStatus], (error, users) => {
      if (error) {
        response.send(error);
      } else {
        response.send(users);
      }
    });
  }
);

router.delete("/deleteLocationById/:locationId", (request, response) => {
  const { locationId } = request.params;
  const statement = `
      DELETE FROM 
        location_details
      WHERE 
        location_id = ?
`;
  // db.pool.query(statement, [locationId], (error, users) => {
  //   if (error) {
  //     response.send(error);
  //   } else {
  //     response.send(users);
  //   }
  // });
  db.pool.query(statement, [locationId], (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.delete("/deleteDomainById/:domainId", (request, response) => {
  const { domainId } = request.params;
  const statement = `
      DELETE FROM 
        domain_details
      WHERE 
        domain_id = ?
`;
  // db.pool.query(statement, [domainId], (error, users) => {
  //   if (error) {
  //     response.send(error);
  //   } else {
  //     response.send(users);
  //   }
  // });
  db.pool.query(statement, [domainId], (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.delete("/deleteDesignationById/:designationId", (request, response) => {
  const { designationId } = request.params;
  const statement = `
      DELETE FROM 
        designation_details
      WHERE 
        designation_id = ?
`;
  // db.pool.query(statement, [designationId], (error, users) => {
  //   if (error) {
  //     response.send(error);
  //   } else {
  //     response.send(users);
  //   }
  // });
  db.pool.query(statement, [designationId], (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.delete(
  "/deleteRequestStatusById/:requestStatusId",
  (request, response) => {
    const { requestStatusId } = request.params;
    const statement = `
      DELETE FROM 
        request_status_details
      WHERE 
        rq_st_id = ?
`;
    // db.pool.query(statement, [requestStatusId], (error, users) => {
    //   if (error) {
    //     response.send(error);
    //   } else {
    //     response.send(users);
    //   }
    // });
    db.pool.query(statement, [requestStatusId], (error, result) => {
      response.send(utils.createResult(error, result));
    });
  }
);

router.delete(
  "/deleteCandidateStatusById/:candidateStatusId",
  (request, response) => {
    const { candidateStatusId } = request.params;
    const statement = `
      DELETE FROM 
        candidate_status_details
      WHERE 
        cnd_st_id = ?
`;
    // db.pool.query(statement, [candidateStatusId], (error, users) => {
    //   if (error) {
    //     response.send(error);
    //   } else {
    //     response.send(users);
    //   }
    // });
    db.pool.query(statement, [candidateStatusId], (error, result) => {
      response.send(utils.createResult(error, result));
    });
  }
);

module.exports = router;
